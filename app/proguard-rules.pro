#
## If your project uses WebView with JS, uncomment the following
## and specify the fully qualified class name to the JavaScript interface
## class:
##-keepclassmembers class fqcn.of.javascript.interface.for.webview {
##   public *;
##}
#
## Uncomment this to preserve the line number information for
## debugging stack traces.
-keepattributes SourceFile,LineNumberTable
#
## If you keep the line number information, uncomment this to
## hide the original source file name.
-renamesourcefileattribute SourceFile

# Add any project specific keep options here:

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}
-keep class android.support.v4.** { *; }
-keep interface android.support.v4.** { *; }

-keep class android.support.v7.** { *; }
-keep interface android.support.v7.** { *; }
-keep public class * extends android.app.Activity




-dontwarn okio.**
-dontskipnonpubliclibraryclasses
-dontobfuscate
-forceprocessing
-optimizationpasses 5
-keepattributes Annotation,Signature,InnerClasses,EnclosingMethod
-dontwarn InnerClasses
#-assumenosideeffects class android.util.Log {
#    public static * d(...);
#    public static * v(...);
#     public static * e(...);
#}



-keeppackagenames model
-keeppackagenames view
-keep public class *
-keepnames class ** { *; }

-dontwarn okio.**
-dontskipnonpubliclibraryclasses
-dontobfuscate
-forceprocessing
-optimizationpasses 5
-keepattributes Annotation,Signature,InnerClasses,EnclosingMethod
-dontwarn InnerClasses
#-assumenosideeffects class android.util.Log {
#    public static * d(...);
#    public static * v(...);
#     public static * e(...);
#}

-keepattributes *Annotation*
-keepclassmembers class ** {
    @org.greenrobot.eventbus.Subscribe <methods>;
}
-keep enum org.greenrobot.eventbus.ThreadMode { *; }

-keepclassmembers class * {
    @android.webkit.JavascriptInterface <methods>;
}


# Gson specific classes
-keep class sun.misc.Unsafe { *; }

-keep public class com.octopus_k_12.base.** {*;}
-keep public class com.octopus_k_12.momentz.** {*;}
-keep public class com.octopus_k_12.room.converters.** {*;}
-keep public class com.octopus_k_12.room.dao.** {*;}
-keep public class com.octopus_k_12.room.entities.** {*;}
-keep public class com.octopus_k_12.room.model.** {*;}
-keep public class com.octopus_k_12.room.repository.** {*;}


-keep public class com.octopus_k_12.view.home.model.local.** {*;}

-keep public class com.view.model.signup.local.** {*;}
-keep public class com.view.model.signup.remote.request.** {*;}
-keep public class com.view.model.signup.remote.response.** {*;}

-keep public class com.network.model.errorresponse.** {*;}
-keep public class com.network.model.** {*;}



-keepattributes JavascriptInterface
-keepattributes *Annotation*

-dontwarn com.razorpay.**
-keep class com.razorpay.** {*;}

-optimizations !method/inlining/*

-keepclasseswithmembers class * {
  public void onPayment*(...);
}