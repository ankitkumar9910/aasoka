package com.view.model.signup.view

import android.content.Context
import android.graphics.drawable.Drawable
import android.text.InputFilter
import android.text.InputType
import android.util.AttributeSet
import android.view.Gravity
import android.view.ViewGroup
import android.widget.LinearLayout
import androidx.appcompat.widget.AppCompatEditText
import androidx.core.content.ContextCompat
import androidx.core.content.ContextCompat.getColor
import com.network.extension.dipToPixels
import com.network.helper.OtpFilledCallback
import com.network.helper.PinOnKeyListener
import com.network.helper.PinTextWatcher
import com.octopus_k_12.R
import kotlin.properties.Delegates


class OtpEditTexts : LinearLayout {
    private lateinit var editTextbackgroundDrawable: Drawable
    private var textColor: Int by Delegates.notNull()
    private var inputType: Int by Delegates.notNull()
    private var otpFilledCallback:OtpFilledCallback? =null

    constructor(context: Context) : super(context) {
        init(null)
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        init(attrs)
    }

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        init(attrs)
    }

    private fun init(attrs: AttributeSet?) {
        val ta = context.obtainStyledAttributes(attrs, R.styleable.OtpEditTexts)
        editTextbackgroundDrawable = ta.getDrawable(R.styleable.OtpEditTexts_background_drawable)
            ?: ContextCompat.getDrawable(context.applicationContext, R.drawable.rectangular_bordered_bg)!!
        textColor = ta.getColor(R.styleable.OtpEditTexts_text_color, getColor(context, R.color.colorBlack))
        inputType = ta.getInt(R.styleable.OtpEditTexts_input_type, InputType.TYPE_CLASS_NUMBER)
        ta.recycle()
    }


    private lateinit var editTexts: Array<AppCompatEditText>

    fun addEditTexts(numEditText: Int, otpFilledCallback:OtpFilledCallback) {
        this.otpFilledCallback = otpFilledCallback
        removeAllViews()
        weightSum = numEditText.toFloat()
        gravity = Gravity.CENTER
        editTexts = Array(numEditText) {
            val editText = AppCompatEditText(context)
            editText.inputType = inputType
            editText.filters = editText.filters + InputFilter.AllCaps()
            val p = LayoutParams(
                context?.dipToPixels(0f)!!,
                ViewGroup.LayoutParams.WRAP_CONTENT,
                1f
            )
            p.marginStart = context?.dipToPixels(8f)!!
            p.marginEnd = context?.dipToPixels(8f)!!
            // editText.setPadding(0, context?.dipToPixels(18f)!!, 0, context?.dipToPixels(18f)!!)
            editText.layoutParams = p
            editText.id = it
            editText.setTextColor(textColor)
            editText.background = editTextbackgroundDrawable
            editText.gravity = Gravity.CENTER
            addView(editText)
            editText
        }
        editTexts.forEachIndexed { index, appCompatEditText ->
            appCompatEditText.addTextChangedListener(PinTextWatcher(index, editTexts, otpFilledCallback))
            appCompatEditText.setOnKeyListener(PinOnKeyListener(index, editTexts))
        }
        editTexts[0].requestFocus()
    }


    fun getText(): StringBuilder {
        val text = java.lang.StringBuilder()
        editTexts.forEach {
            text.append(it.text.toString())
        }
        return text
    }

    fun setText(otp: String){
        var index = 0
        editTexts.forEach {
            it.setText(otp[index].toString())
            index += 1
        }
    }

}

