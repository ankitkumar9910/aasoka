package com.network.utils

class Constants {
    companion object {

        const val REFERRAL_CODE: String = "referralCode"
        const val WITH_DECIMAL_FORMAT = "##,##,##0.00"
        const val PHONE_NUMBER_PREFIX = "+91"
        const val PHONE_NUMBER = "phone_number"
        const val CATEGORY_NAME: String = "category_name"
        const val ACCESS_TOKEN: String = "access_token"
        const val USER_ID: String = "user_id"
        const val USER_NAME: String = "user_name"
        const val USER_NUMBER: String = "user_number"
        const val USER_CLASS_ID: String = "user_class_id"
        const val USER_TYPE: String = "user_type"
        const val USER_ADDRESS: String = "user_address"
        const val USER_SCHOOL: String = "user_school"
        const val USER_EMAIL: String = "user_email"
        const val USER_GENDER: String = "user_gender"
        const val USER_DOB: String = "user_dob"
        const val CAT_TAB_ID: String = "cat_tab_id"
        const val COME_FROM: String = "comeFrom"
        const val COME_FROM_NOTIFICATION: String = "comeFromNotification"
        const val NOTIFICATION_TYPE: String = "notificationType"
        const val NOTIFICATION_TYPE_ORDER_STATUS: String = "order_status"
        const val ORDER_ID: String = "orderId"
        const val CUSTOM_NOTIFICATION_URL: String = "notification_url"
        const val IMAGE_ID: String = "image_id"
        const val IMAGE_NAME: String = "image_name"
        const val IMAGE_VIEW: String = "image_view"
        const val IMAGE_BASE_URL: String = "image_base_url"
        const val IMAGE_TYPE: String = "image_type"
        const val IMAGE_URL: String = "image_url"
        const val IS_FROM_START = "isFromStart"
        const val ASSIGNMENT_DETAIL = "AssignmentDetail"
        const val DOC = "Doc"
        const val IMAGE = "Image"
        const val OPEN_SCREEN_TYPE = "openScreenType"
        const val OPEN_SCREEN_TYPE_USER_EDIT = "userEdit"
        const val BOARD_ID = "board_id"
        const val CHAT_NOTIFICATION = "notification"
        const val LIVE_CLASS_URL = "liveClassUrl"
        const val INTERNET_ERROR = 101
        const val FILE_NOT_FOUND = 404
        const val HOME_TABS = "home"
        const val PROGRESS_TABS = "progress"
        const val EBOOK_TABS = "ebook"
        const val ASSESSMENT_TABS = "assessment"
        const val ASSESSMENT_ID = "assessmentId"
        const val ASSIGNMENT_ID = "assignmentId"
        const val ASSESSMENT_NAME = "assignmentNAME"
        const val NCERT = "ncert"
        const val QUESTION_ID = "questionId"
        const val MORE_TABS = "more"
        const val MOBILE_NUMBER = "mobileNumber"
        const val COUNTRY_CODE = "countryCode"
        const val RESULT_ID = "resultId"
        const val OS_TYPE = "ostype"
        const val DEVICE_ID = "deviceID"
        const val DEVICE_BRAND = "devicebrand"
        const val DEVICE_MODEL = "devicemodel"
        const val APP_VERSION = "appversion"
        const val AUTHORIZATION = "Authorization"
        const val ANDROID = "android"
        const val UPDATE_DESCRIPTION = "update_description"
        const val END_DATE = "endDate"
        const val DURATION = "duration"
        const val TOTAL_MARKS = "totalMarks"
        const val NUMBER_QUESTION = "numberQuestion"
        const val QUIZ_NAME = "quizName"
        const val PARENT = "PARENT"
        const val TEACHER = "TEACHER"
        const val VIDEO_URL = "videoUrl"
        const val RID = "rid"
        const val ENTRY_DATE = "entryDate"
        const val SUBJECT_ID = "subjectId"
        const val SUBJECT_NAME = "subjectName"
        const val CHAPTER_NAME = "chapterName"
        const val CHAPTER_Id = "chapterId"
        const val BOOK_ID = "bookId"
        const val SUBJECT_URL = "subjectUrl"
        const val EBOOK_URL = "ebookUrl"

        //        const val CONTACTS_PERMISSION_REQUEST_CODE: Int = 100
    }

    abstract class TABS private constructor() {
        init {
            throw IllegalStateException("TABS class")
        }

        companion object {
            val HOME_TABS = 0
            val NCERT_TABS = 1
            val EBOOK_TABS = 1
            val ASSESSMENT_TABS = 2
            val MORE_TABS = 3
            val HOME_TAB = "HOME_TABS"
            val PROGRESS_TAB = "PROGRESS_TABS"
            val EBOOK_TAB = "EBOOK_TABS"
            val ASSESSMENT_TAB = "ASSESSMENT_TABS"
            val MORE_TAB = "MORE_TABS"
        }
    }




}