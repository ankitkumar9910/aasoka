package com.network.retrofit.retroApi

import android.content.SharedPreferences
import com.network.model.ServerResponse
import com.network.utils.Constants
import com.view.model.signup.remote.request.VerifyOtpRequest
import kotlinx.coroutines.Deferred
import com.octopus_k_12.room.entities.User

import retrofit2.Response

class OtpRepositoryImpl(private val verifyLoginApi: VerifyLoginApi, private val sharedPreferences: SharedPreferences) :
    OtpRepository {

    override fun saveAccessToken(accessToken: String) {
        val editor = sharedPreferences.edit()
        editor.putString(Constants.ACCESS_TOKEN, accessToken)
        editor.apply()
    }

    override fun saveUserIdToken(userId: String) {
        val editor = sharedPreferences.edit()
        editor.putString(Constants.USER_ID, userId)
        editor.apply()
    }

    override fun verifyOtpAsync(
        phoneNumber: String,
        otp: String,oldUserId:String
    ): Deferred<Response<ServerResponse<List<User>>>> {
        return verifyLoginApi.verifyOtpAsync(
            VerifyOtpRequest(
                phoneNumber = phoneNumber,
                otp = otp, oldUserId=oldUserId
            )
        )
    }


    override fun saveSharedPreferenceData(key: String, value: String) {
        val editor = sharedPreferences.edit()
        editor.putString(key, value)
        editor.apply()
    }
}