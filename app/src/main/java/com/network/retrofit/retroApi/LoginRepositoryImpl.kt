package com.network.retrofit.retroApi

import android.content.SharedPreferences
import com.network.model.ServerResponse
import com.network.utils.Constants
import com.view.model.signup.remote.request.SendMessageRequest
import com.view.model.signup.remote.response.GetConfigResponse
import com.view.model.signup.remote.response.SendMessageResponse
import kotlinx.coroutines.Deferred
import retrofit2.Response

class LoginRepositoryImpl(private val loginApi: SendOtpApi, val sharedPreferences: SharedPreferences) :
    LoginRepository {
    override fun getSharedPreferencesString(key: String, default: String): String {
        return sharedPreferences.getString(key, default)?:""
    }

    override fun setSharedPreferences(key: String, value: String) {
        val editor = sharedPreferences.edit()
        editor.putString(key, value)
        editor.apply()
    }

    override fun sendMessageAsync(phoneNumber: String,otp:String): Deferred<Response<ServerResponse<SendMessageResponse>>> {
            return loginApi.sendMessageAsync(
            SendMessageRequest(phoneNumber = phoneNumber,otp = otp))
    }

    override fun getConfigApi(): Deferred<Response<ServerResponse<GetConfigResponse>>> {
        return loginApi.getConfigApi("retailapp")
    }


    override fun getConfigPhoneApi(): Deferred<Response<ServerResponse<GetConfigResponse>>> {
        return loginApi.getConfigPhoneApi("retailapp",sharedPreferences.getString(Constants.USER_NUMBER,"")!!)
    }

}