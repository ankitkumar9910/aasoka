package com.network.retrofit.retroApi

import com.network.model.ServerResponse
import com.view.model.signup.remote.request.VerifyOtpRequest
import kotlinx.coroutines.Deferred
import com.octopus_k_12.room.entities.User
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.POST

interface VerifyLoginApi {
    @POST("api/getlogindata")
    fun verifyOtpAsync(@Body otpRequest: VerifyOtpRequest): Deferred<Response<ServerResponse<List<User>>>>
}