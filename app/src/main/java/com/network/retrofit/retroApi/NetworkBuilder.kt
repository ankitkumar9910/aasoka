package com.network.retrofit.retroApi

import android.content.SharedPreferences
import android.os.Build
import com.network.utils.Constants
import com.octopus_k_12.BuildConfig
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import java.util.concurrent.TimeUnit

object NetworkBuilder {

    const val BASE_URL = BuildConfig.BASE_URL


    fun <T> create(baseUrl: String, apiType: Class<T>,
        sharedPreferences: SharedPreferences,deviceId : String) =
        Retrofit.Builder()
            .baseUrl(baseUrl)
            .client(gmHttpClient(sharedPreferences, deviceId))
            .addCallAdapterFactory(CoroutineCallAdapterFactory.invoke())
            .addConverterFactory(MoshiConverterFactory.create())//KotlinJsonAdapterFactory
            .build()
            .create(apiType)

    private fun gmHttpClient(sharedPreferences: SharedPreferences, deviceId : String): OkHttpClient {
        val builder = OkHttpClient.Builder()
            .connectTimeout(1, TimeUnit.MINUTES)
            .writeTimeout(1, TimeUnit.MINUTES)
            .readTimeout(1, TimeUnit.MINUTES)
            .addInterceptor(HttpLoggingInterceptor().apply {
//                this.level = HttpLoggingInterceptor.Level.BODY
            }).retryOnConnectionFailure(true)

            val token = sharedPreferences.getString(Constants.ACCESS_TOKEN, "") ?: ""
            if (token.isNotEmpty()) {

                builder.addInterceptor { chain ->
                    var request = chain.request()
                    val requestBuilder = request.newBuilder()
                        .header(Constants.AUTHORIZATION, String.format("%s %s", "Bearer", token))
                        .header(Constants.OS_TYPE, Constants.ANDROID)
                        .header(Constants.DEVICE_ID,deviceId)
                        .header(Constants.DEVICE_BRAND, Build.BRAND)
                        .header(Constants.DEVICE_MODEL, Build.MODEL)
                        .header(Constants.APP_VERSION, BuildConfig.VERSION_NAME)
                         request = requestBuilder.build()
                         chain.proceed(request)
                }
            }

        return builder.build()
    }
}