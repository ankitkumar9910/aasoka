package com.network.retrofit.retroApi

import com.network.model.ServerResponse
import com.view.model.signup.remote.request.SendMessageRequest
import com.view.model.signup.remote.request.VerifyTruecallerRequest
import com.view.model.signup.remote.response.GetConfigResponse
import com.view.model.signup.remote.response.SendMessageResponse
import kotlinx.coroutines.Deferred
import com.octopus_k_12.room.entities.User
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Query

interface SendOtpApi {

    @POST("api/getapiotp")
    fun sendMessageAsync(@Body sendMessageRequest: SendMessageRequest): Deferred<Response<ServerResponse<SendMessageResponse>>>

    @POST("verify_truecaller")
    fun verify_truecaller(@Body sendMessageRequest: VerifyTruecallerRequest): Deferred<Response<ServerResponse<User>>>
    @GET("get_config")
    fun getConfigApi(@Query("key") key: String): Deferred<Response<ServerResponse<GetConfigResponse>>>

    @GET("v1/get_config")
    fun getConfigPhoneApi(@Query("key") key: String,@Query("mobile") phone: String): Deferred<Response<ServerResponse<GetConfigResponse>>>
}