package com.network.retrofit.retroApi

import com.network.model.ServerResponse
import com.view.model.signup.remote.response.SendMessageResponse
import kotlinx.coroutines.Deferred
import com.octopus_k_12.room.entities.User
import com.view.model.home.*
import retrofit2.Response

interface ApiRepository {
    /*
    *  Login Page api
    */
    fun sendMessageAsync(phoneNumber: String, hash: String): Deferred<Response<ServerResponse<SendMessageResponse>>>
    fun dashboardApi(userId: Int): Deferred<Response<ServerResponse<DashboardModel>>>
    fun userRegistration( request: RegistrationModelRequest): Deferred<Response<ServerResponse<User>>>

    fun getSharedPreferencesString(key: String, default: String): String
    fun setSharedPreferences(key: String, value: String)
    fun getapisubtopic(request: GetTopicRequest): Deferred<Response<ServerResponse<List<GetTopicResponse>>>>
    fun getUserConfigApi( request: HomeRequest): Deferred<Response<ServerResponse<UserConfigResponse>>>
    fun trackcontentapi( request: WatchVideoRequest): Deferred<Response<ServerResponse<String>>>
    fun clearAllSharedPrefrences()
    /*
    *  Subscribe User API
    */

    /*
     *  Home Page API
    */

}
