package com.network.retrofit.retroApi

import com.network.model.ServerResponse
import com.view.model.signup.remote.response.GetConfigResponse
import com.view.model.signup.remote.response.SendMessageResponse
import kotlinx.coroutines.Deferred
import retrofit2.Response


interface LoginRepository {
    fun sendMessageAsync(phoneNumber: String,otp:String): Deferred<Response<ServerResponse<SendMessageResponse>>>
    fun getSharedPreferencesString(key: String, default: String): String
    fun setSharedPreferences(key: String, value: String)
    fun getConfigApi(): Deferred<Response<ServerResponse<GetConfigResponse>>>
       fun getConfigPhoneApi(): Deferred<Response<ServerResponse<GetConfigResponse>>>
}