package com.network.retrofit.retroApi

import com.network.model.ServerResponse

import com.view.model.signup.remote.request.SendMessageRequest
import com.view.model.signup.remote.response.SendMessageResponse
import kotlinx.coroutines.Deferred
import com.octopus_k_12.room.entities.User
import com.view.model.home.*


import retrofit2.Response
import retrofit2.http.*

interface Api {

    /*
    * Login Api
    */
    @POST("send_otp")
    fun sendMessageAsync(@Body sendMessageRequest: SendMessageRequest): Deferred<Response<ServerResponse<SendMessageResponse>>>
    @POST("api/dashboardapi")
    fun dashboardApi(@Body homeRequest: HomeRequest): Deferred<Response<ServerResponse<DashboardModel>>>
     @POST("api/getregisterdata")
    fun userRegistration(@Body request: RegistrationModelRequest): Deferred<Response<ServerResponse<User>>>
    @POST("api/getapisubtopic")
    fun getapisubtopic(@Body request: GetTopicRequest): Deferred<Response<ServerResponse<List<GetTopicResponse>>>>
    @POST("api/userconfigapi")
    fun getUserConfigApi(@Body request: HomeRequest): Deferred<Response<ServerResponse<UserConfigResponse>>>
      @POST("api/trackcontentapi")
    fun trackcontentapi(@Body request: WatchVideoRequest): Deferred<Response<ServerResponse<String>>>

}