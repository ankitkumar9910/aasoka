package com.network.retrofit.retroApi

import android.content.SharedPreferences
import com.network.model.ServerResponse
import kotlinx.coroutines.Deferred
import com.octopus_k_12.room.dao.DBQuery
import com.octopus_k_12.room.entities.User
import com.view.model.signup.remote.request.SendMessageRequest
import com.view.model.signup.remote.response.SendMessageResponse
import com.view.model.home.*
import retrofit2.Response
import retrofit2.http.Body

class ApiRepositoryImpl(private val api: Api, private val dbQuery: DBQuery, private val sharedPreferences: SharedPreferences) : ApiRepository {
    /*
     * Bottom Page API calling
     */
    override fun sendMessageAsync(phoneNumber: String, hash: String): Deferred<Response<ServerResponse<SendMessageResponse>>> {
        return api.sendMessageAsync(SendMessageRequest(phoneNumber = phoneNumber,otp=""))
    }
    override fun dashboardApi(userId: Int): Deferred<Response<ServerResponse<DashboardModel>>> {
        return api.dashboardApi(HomeRequest( userId))
    }
      override fun getSharedPreferencesString(key: String, default: String): String {
        return sharedPreferences.getString(key, default)?:""
    }
   override fun getUserConfigApi(request: HomeRequest): Deferred<Response<ServerResponse<UserConfigResponse>>> {

        return api.getUserConfigApi(request)
    }


    override fun trackcontentapi(request: WatchVideoRequest): Deferred<Response<ServerResponse<String>>> {
      return api.trackcontentapi(request)
    }


    override  fun userRegistration( request: RegistrationModelRequest): Deferred<Response<ServerResponse<User>>>  {
        return api.userRegistration(request)
    }




    override fun getapisubtopic(request: GetTopicRequest): Deferred<Response<ServerResponse<List<GetTopicResponse>>>> {
       return api.getapisubtopic(request)
    }





    override fun setSharedPreferences(key: String, value: String) {
        val editor = sharedPreferences.edit()
        editor.putString(key, value)
        editor.apply()
    }

    override fun clearAllSharedPrefrences() {
        val editor = sharedPreferences.edit()
        editor.clear()
        editor.apply()
        // sharedPreferences.erase()
    }
   }