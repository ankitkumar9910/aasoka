package com.network.retrofit.retroApi

import com.network.model.ServerResponse
import kotlinx.coroutines.Deferred
import com.octopus_k_12.room.entities.User

import retrofit2.Response

interface OtpRepository {
    fun verifyOtpAsync(
        phoneNumber: String,
        otp: String,oldUserId:String
    ): Deferred<Response<ServerResponse<List<User>>>>

    fun saveAccessToken(accessToken: String)
    fun saveUserIdToken(userId: String)
    fun saveSharedPreferenceData(key: String, value: String)

}