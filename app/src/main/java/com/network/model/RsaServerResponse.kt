package com.network.model

import com.squareup.moshi.Json

data class RsaServerResponse<T>(
    @field:Json(name= "reason") val reason: String = "",
    @field:Json(name= "success") val success: Boolean = true,
    @field:Json(name= "data") val data:T
)