package com.network.model.errorresponse

import com.squareup.moshi.Json

data class SingleErrorResponse(
    @field:Json(name= "error") val error: String
)