package com.network.model.errorresponse

import com.squareup.moshi.Json

data class DynamicErrorResponse(
    @field:Json(name= "errors") val errors: Map<String, List<String>>
)