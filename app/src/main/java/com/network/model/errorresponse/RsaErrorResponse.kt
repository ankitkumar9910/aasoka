package com.network.model.errorresponse

import com.squareup.moshi.Json

data class
RsaErrorResponse(
    @field:Json(name= "success") val success: Boolean?,
    @field:Json(name= "reason") val reason: String
)