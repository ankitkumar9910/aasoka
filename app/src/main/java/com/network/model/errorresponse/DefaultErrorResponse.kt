package com.network.model.errorresponse

import com.squareup.moshi.Json

data class
DefaultErrorResponse(
    @field:Json(name= "type") val type: String?,
    @field:Json(name= "message") val message: String
)