package com.network.model.errorresponse

import com.squareup.moshi.Json


/*
{
    "errors": [
        {
            "status": 422,
            "code": 40001,
            "title": "Could not be processed",
            "message": "Note tidak boleh kosong, Requested amount Total biaya tidak boleh lebih besar dari sisa saldo klaim",
            "detail": "Parameter is wrong"
        }
    ]
}
 */
data class JsonApiErrorResponse(
        @field:Json(name= "errors") val errorList: List<Error>
) {
    data class Error(
        @field:Json(name= "status") val status: Int,
        @field:Json(name= "code") val code: Int,
        @field:Json(name= "title") val title: String,
        @field:Json(name= "message") val message: String,
        @field:Json(name= "detail") val detail: String
    )
}