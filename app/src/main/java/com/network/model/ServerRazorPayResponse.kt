package com.network.model

import com.squareup.moshi.Json

data class ServerRazorPayResponse<T> (
    @field:Json(name= "status") val status: Boolean,
    @field:Json(name= "message") val message: String,
    @field:Json(name= "data") val data: String,
    @field:Json(name= "error") val error: String?="",
    @field:Json(name= "errorCode") val errorCode: Int,
    @field:Json(name= "razorpay_order") val razorpayOrder: T
)