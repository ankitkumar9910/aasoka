package com.network.common

sealed class ResultState {
    data class Success<out T>(val data: T?) : ResultState()
    data class Failure(val errorMessage: String, val errorCode: Int) : ResultState()

}