package com.network.extension

import com.network.exception.RequestException

fun Throwable.throwException(): Nothing = throw this

val Throwable.errorCode
    get() = when (this) {
        is RequestException -> statusCode
        else -> null
    }