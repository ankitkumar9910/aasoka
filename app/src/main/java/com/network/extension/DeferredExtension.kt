package com.network.extension
import com.network.common.Result
import com.network.model.RsaServerResponse
import com.network.model.ServerRazorPayResponse
import com.network.model.ServerResponse
import com.network.model.errorresponse.*
import com.network.utils.Constants
import com.squareup.moshi.Moshi
import kotlinx.coroutines.Deferred
import retrofit2.Response
import timber.log.Timber
import java.net.HttpURLConnection
import java.net.SocketTimeoutException
import java.net.UnknownHostException

suspend inline fun <T> Deferred<Response<T>>.awaitAndGet(): Result<T> {
    return try {
        val response = await()

        if (response.isSuccessful) {
            if (response.body() is ServerResponse<*>) {
                val result = response.body() as ServerResponse<*>
                if (result.status) {
                    Result.Success(response.body(), response.code()) as Result<T>
                } else if (result.error!=null){
                    Result.Failure(result.error, result.errorCode)
                } else{
                    Result.Failure(result.message, result.errorCode)
                }
            } else if(response.body() is RsaServerResponse<*>){

                val result = response.body() as RsaServerResponse<*>

                if (result.success) {
                    Result.Success(response.body(), response.code()) as Result<T>
                }else{
                    Result.Failure(result.reason, 0)
                }

            }else if(response.body() is ServerRazorPayResponse<*>){

                val result = response.body() as ServerRazorPayResponse<*>

                if (result.status) {
                    Result.Success(response.body(), response.code()) as Result<T>
                } else if (result.error!=null){
                    Result.Failure(result.error, result.errorCode!!)
                } else{
                    Result.Failure(result.message, result.errorCode!!)
                }

            }
            else {
                Result.Success(response.body(), response.code()) as Result<T>
            }

        } else {

            val errorBody = response.errorBody()?.source()?.readUtf8()
            Timber.e(errorBody)
            val moshi = Moshi.Builder().build()

            val rsaJsonAdapter = moshi.adapter(RsaErrorResponse::class.java)
            val rsaErrorResponse = try{
                rsaJsonAdapter.fromJson(errorBody)?.reason
            }catch (e: Exception){
                null
            }

            val jsonAdapter = moshi.adapter(DefaultErrorResponse::class.java)
            val errorResponse = try {
                jsonAdapter.fromJson(errorBody)?.message
            } catch (e: Exception) {
                null
            }

            val jsonDynamicAdapter = moshi.adapter(DynamicErrorResponse::class.java)
            val jsonDynamicErrorResponse = try {
                jsonDynamicAdapter.fromJson(errorBody)?.errors?.values?.flatten()?.joinToString(", ")
            } catch (e: Exception) {
                null
            }

            val jsonSingleAdapter = moshi.adapter(SingleErrorResponse::class.java)
            val jsonSingleErrorResponse = try {
                jsonSingleAdapter.fromJson(errorBody)?.error
            } catch (e: Exception) {
                null
            }

            val jsonApiAdapter = moshi.adapter(JsonApiErrorResponse::class.java)
            val jsonApiErrorResponse = try {
                jsonApiAdapter.fromJson(errorBody)?.errorList?.get(0)?.message
            } catch (e: Exception) {
                null
            }

            Result.Failure(
                try {
                    rsaErrorResponse ?: errorResponse ?: jsonDynamicErrorResponse ?: jsonSingleErrorResponse ?: jsonApiErrorResponse
                    ?: "Internal Server Error"
                } catch (e: SocketTimeoutException) {
                    "Timeout"
                }, response.code()
            )

        }
    } catch (e: UnknownHostException) {
        Result.Failure("Failed to connect to server= "+e.toString(), Constants.INTERNET_ERROR)
    } catch (e: SocketTimeoutException) {
        Result.Failure("Timeout", HttpURLConnection.HTTP_INTERNAL_ERROR)
    } catch (e: Exception) {
        Timber.tag("Network Request").e(e)
        Result.Failure(e.toString(), HttpURLConnection.HTTP_INTERNAL_ERROR)
    }
}
