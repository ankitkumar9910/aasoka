package com.network.extension

import android.app.Activity
import android.content.res.Resources
import androidx.appcompat.app.AppCompatActivity
import com.network.helper.KeyboardUtil

fun AppCompatActivity.hideKeyboard() = KeyboardUtil.hideKeyboard(this)
fun AppCompatActivity.showKeyboard() = KeyboardUtil.showKeyboard(this)

fun getScreenHeight() = Resources.getSystem().displayMetrics.heightPixels
fun getScreenWidth() = Resources.getSystem().displayMetrics.widthPixels

fun Activity.getStatusBarHeight(): Int {
    var result = 0
    val resourceId = resources.getIdentifier("status_bar_height", "dimen", "android")
    if (resourceId > 0) {
        result = resources.getDimensionPixelSize(resourceId)
    }
    return result
}