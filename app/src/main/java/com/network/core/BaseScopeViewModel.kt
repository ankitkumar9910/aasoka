package com.network.core

import android.app.Application
import kotlinx.coroutines.*
import kotlin.coroutines.CoroutineContext
import kotlin.coroutines.EmptyCoroutineContext

abstract class BaseScopeViewModel(app: Application) : BaseViewModel(app), CoroutineScope {

    private val failedJobIdList = mutableSetOf<String>()

    private val job = Job()

    override val coroutineContext: CoroutineContext
        get() = Dispatchers.IO + job

    override fun onCleared() {
        super.onCleared()
        failedJobIdList.clear()
        job.cancel()
    }

    private fun addJobId(jobId: String) {
        failedJobIdList.add(jobId)
    }

    private fun removeJobId(jobId: String) {
        failedJobIdList.remove(jobId)
    }

    protected fun CoroutineScope.launch(context: CoroutineContext = EmptyCoroutineContext, start: CoroutineStart = CoroutineStart.DEFAULT, doOnComplete: (throwable: Throwable?) -> Unit = {}, id: String, block: suspend CoroutineScope.() -> Unit): Job {
        return launch(context, start, block).apply {
                invokeOnCompletion {
                    if (it != null) addJobId(id)
                    else removeJobId(id)
                    doOnComplete(it)
                }
            }
    }

    protected fun <T> CoroutineScope.async(context: CoroutineContext = EmptyCoroutineContext, start: CoroutineStart = CoroutineStart.DEFAULT, doOnComplete: (throwable: Throwable?) -> Unit = {}, id: String, block: suspend CoroutineScope.() -> T): Deferred<T> {
        return async(context, start, block).apply {
                invokeOnCompletion {
                    if (it != null) addJobId(id)
                    else removeJobId(id)
                    doOnComplete(it)
                }
            }
    }

    protected fun addToFailedJobStack() {
        coroutineContext.cancel()
    }

    override fun onConnectedToNetwork() {
        super.onConnectedToNetwork()
        resumeFailedJob(failedJobIdList)
    }

    open fun resumeFailedJob(jobIdList: Iterable<String>) {}
}