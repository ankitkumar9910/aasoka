package com.network.core

import android.Manifest
import android.os.Build
import android.os.Bundle
import android.view.*
import android.widget.ProgressBar
import androidx.annotation.ColorRes
import androidx.annotation.LayoutRes
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView
import androidx.fragment.app.Fragment
import com.network.extension.getColorFromRes
import com.network.helper.KeyboardUtil
import com.ui.PageLoadingProgress
import com.octopus_k_12.R
import com.crashlytics.android.Crashlytics
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.ThreadMode
import org.greenrobot.eventbus.Subscribe

abstract class BaseFragment<VM : BaseViewModel> : Fragment(), View.OnClickListener {
    companion object {
        const val CAMERA_REQUEST_CODE: Int = 12
    }

    @LayoutRes
    abstract fun getLayoutRes(): Int

    @ColorRes
    open fun statusBarColor(): Int = R.color.colorGray

    open fun lightColorIcon(): Boolean = false
    abstract val viewModel: VM
    fun fragmentTag(): String = this::class.java.simpleName
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(getLayoutRes(), container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        changeStatusBarColor()
        changeStatusBarIconColor()
        viewInitialization(view)
        onPreparationFinished(view)
        viewModel.doOnViewAttached()
    }


    override fun onStart() {
        super.onStart()
        EventBus.getDefault().register(this)
    }

    override fun onStop() {
        super.onStop()
        EventBus.getDefault().unregister(this)
    }

    protected fun showLoading(isShow: Boolean) {
        (activity as BaseActivity<*>?)?.showLoading(isShow)
    }

    fun changeStatusBarColor(@ColorRes barColor: Int = statusBarColor()) {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) return
        activity?.window?.apply {
            clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
            addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
            statusBarColor().let { statusBarColor = context.getColorFromRes(barColor) }
        }
    }

    fun showBackButton() {
        (activity as BaseActivity<*>).supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }

    fun showBackButton(show: Boolean) {
        (activity as BaseActivity<*>).supportActionBar?.setDisplayHomeAsUpEnabled(show)
    }

    fun hideBackButton() {
        (activity as BaseActivity<*>).supportActionBar?.setDisplayHomeAsUpEnabled(false)
    }

    fun setToolBarTitle(title: String) {
        (activity as BaseActivity<*>).supportActionBar?.title = title
    }

    fun setToolBarSubTitle(subtitle: String) {
        (activity as BaseActivity<*>).supportActionBar?.subtitle = subtitle
    }

    private fun changeStatusBarIconColor() {
        activity?.window?.decorView?.rootView?.systemUiVisibility = if (lightColorIcon()) 0 else 8192
    }

    override fun onClick(view: View?) {
        view?.let { clickView ->
          /*  if (clickView.id == R.id.toolBarBack) {
                popBackStack()
            }*/
        }
    }

    /**
     * This method is called after view has been created.
     * This method should be used to initialize all views that are needed to be created (and recreated after fragment is reattached)
     * @param view The root view of the fragment
     */
    open fun viewInitialization(view: View) {}

    /**
     * This method is called after viewInitialization(view) is finished
     * @param view The root view of the fragment
     */
    open fun onPreparationFinished(view: View) {}

    open fun onNetworkRequestFailure(errorCode: Int, errorMap: Map<String, String>) {}

    @Subscribe(threadMode = ThreadMode.MAIN)
    open fun onMessageEvent(event: BaseEventBusModel) {
    }

    var pageLoading: PageLoadingProgress? = null


    fun showProgressBar(value: Boolean) {
        if (pageLoading != null) {
            pageLoading!!.visibility = if (value) View.VISIBLE else View.GONE
        }
    }



    /***
     * override this and return true if child is handling backpress (Toolbar and
     * device back button)
     */
    open fun onPopBackStack(): Boolean {
        return false
    }

    fun popBackStack() {
        fragmentManager?.let { fragMan ->
            try {
                KeyboardUtil.hideKeyboard(activity)
                fragMan.popBackStack()
            } catch (e: Exception) {
                Crashlytics.logException(e)
            }
        }
    }

    private lateinit var roundProgressBar: ProgressBar
    fun setRoundProgressBar() {
        try {
            view?.let {
                this.roundProgressBar = it.findViewById(R.id.roundProgress)
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    fun showRoundProgressBar(value: Boolean) {
        try {
            roundProgressBar.visibility = if (value) View.VISIBLE else View.GONE
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    fun getRoundProgressBar(): Boolean {
        return roundProgressBar.visibility == View.VISIBLE
    }

    protected fun makeRequest() {
        requestPermissions(arrayOf(Manifest.permission.CAMERA), BaseFragment.Companion.CAMERA_REQUEST_CODE)
    }
}