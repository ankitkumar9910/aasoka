package com.network.core

import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.util.TypedValue
import android.view.MenuItem
import android.view.WindowManager
import android.widget.Toast
import androidx.annotation.ColorRes
import androidx.annotation.LayoutRes
import androidx.appcompat.app.AppCompatActivity
import com.airbnb.lottie.utils.Utils
import com.crashlytics.android.Crashlytics
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import com.google.firebase.remoteconfig.FirebaseRemoteConfig
import com.google.firebase.remoteconfig.FirebaseRemoteConfigSettings
import com.network.extension.getColorFromRes
import com.network.extension.hideKeyboard
import com.network.helper.KeyboardUtil
import com.octopus_k_12.R
import com.ui.PageLoadingProgress
import io.fabric.sdk.android.Fabric


abstract class BaseActivity<VM : BaseViewModel> : AppCompatActivity() {
    private var remoteConfig : FirebaseRemoteConfig = FirebaseRemoteConfig.getInstance()

    @LayoutRes
    abstract fun getLayoutRes(): Int

    @ColorRes
    open fun statusBarColor(): Int = R.color.colorGray

    open fun lightColorIcon(): Boolean = false

    private val loadingDialog by lazy { PageLoadingProgress(this) }

    abstract val viewModel: VM

    private val activityEventsListeners : ArrayList<BaseActivity.ActivityEventsListener> = ArrayList()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(getLayoutRes())
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) changeStatusBarColor()
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) changeStatusBarIconColor()
//        if(BuildConfig.IS_FIREBASE_ENABLED) {
            Fabric.with(this, Crashlytics())
//        }
    }

    fun getFirebaseRemotConfig() : FirebaseRemoteConfig {
        return remoteConfig
    }

    fun setRemoteHomePageConfig(key: String ,value : String) {
        val configSettings = FirebaseRemoteConfigSettings.Builder()
            .setMinimumFetchIntervalInSeconds(4200)
            .build()
        getFirebaseRemotConfig().setConfigSettingsAsync(configSettings)
        val map = mutableMapOf<String,String>()
        map[key] = value
        getFirebaseRemotConfig().setDefaultsAsync(map as Map<String, Any>)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                KeyboardUtil.hideKeyboard(this)
                onBackPressed()
            }
        }
        return true
    }

    override fun onBackPressed() {
        if (!loadingDialog.isVisible(this)) super.onBackPressed()
    }

    override fun onDestroy() {

        super.onDestroy()
        hideKeyboard()
    }



    fun showLoading(isShow: Boolean) {
        if (isShow) {
            with(loadingDialog) {
                showFromActivity(this@BaseActivity)
                requestFocus()
            }
            hideKeyboard()
        } else loadingDialog.hideFromActivity(this)
    }

    fun changeStatusBarColor(@ColorRes barColor: Int = statusBarColor()) {
        window.apply {
            clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
            addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
            statusBarColor = getColorFromRes(barColor)
        }
    }

    private fun changeStatusBarIconColor() {
        window.decorView.rootView.systemUiVisibility = if (lightColorIcon()) 0 else 8192
    }

    private fun Context.getTypedValue(res: Int): TypedValue {
        return TypedValue().apply {
            resources.getValue(res, this, true)
        }
    }

    fun showBackButton() {
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }

    fun setToolBarTitle(title:String){
        supportActionBar?.title = title
    }

    interface ActivityEventsListener {
        fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?)
    }

    fun addActivityEventListener(listener: BaseActivity.ActivityEventsListener) {
        activityEventsListeners.add(listener)
    }

    fun removeActivityEventListener(listener: BaseActivity.ActivityEventsListener) {
        activityEventsListeners.remove(listener)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        for (listener in activityEventsListeners) {
            listener.onActivityResult(requestCode, resultCode, data)
        }
    }
    fun getStatus(context: Context){
        FirebaseDatabase.getInstance().getReference("app_status").addValueEventListener(
            object : ValueEventListener {
                override fun onDataChange(dataSnapshot: DataSnapshot) {
            Toast.makeText(context ,dataSnapshot.value.toString(),Toast.LENGTH_LONG).show()
                }
                override fun onCancelled(databaseError: DatabaseError) {}
            })
    }


}