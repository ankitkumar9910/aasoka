package com.network.core

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import androidx.annotation.ColorRes
import androidx.annotation.LayoutRes
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.ViewModel
import com.airbnb.lottie.LottieAnimationView
import com.network.extension.getColorFromRes
import com.network.extension.hideKeyboard
import com.network.helper.KeyboardUtil
import com.octopus_k_12.R
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.octopus_k_12.utils.FragmentFactory
import com.octopus_k_12.utils.FragmentNavigation

abstract class BaseBottomSheetDialogFragment<VM: ViewModel> : BottomSheetDialogFragment(), FragmentNavigation {

    @LayoutRes
    abstract fun getLayoutRes(): Int

    @ColorRes
    open fun statusBarColor(): Int? = null

    open fun lightColorIcon(): Boolean = false

    abstract val viewModel: VM

    fun fragmentTag(): String = this::class.java.simpleName

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(getLayoutRes(), container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        KeyboardUtil(activity as FragmentActivity, view)
        isCancelable = false
        changeStatusBarColor()
        changeStatusBarIconColor()
        viewInitialization(view)
        onPreparationFinished(view)
    }

    override fun onDestroyView() {
        (activity as AppCompatActivity?)?.hideKeyboard()
        super.onDestroyView()
    }

    private fun changeStatusBarColor() {
        activity?.window?.apply {
            clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
            addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
            statusBarColor()?.let { color -> statusBarColor = context.getColorFromRes(color) }
        }
    }

    private fun changeStatusBarIconColor() {
        activity?.window?.decorView?.rootView?.systemUiVisibility = if (lightColorIcon()) 0 else 8192
    }

    /**
     * This method is called after view has been created.
     * This method should be used to initialize all views that are needed to be created (and recreated after fragment is reattached)
     * @param view The root view of the fragment
     */
    open fun viewInitialization(view: View) {}

    /**
     * This method is called after viewInitialization(view) is finished
     * @param view The root view of the fragment
     */
    open fun onPreparationFinished(view: View) {}


    lateinit var roundProgressBar : LottieAnimationView

    fun setRoundProgressBar() {
        try {
            this.roundProgressBar = requireView().findViewById(R.id.roundProgress)
        } catch (e: Exception) {
            //Do nothing
        }
    }

    fun showRoundProgressBar(value: Boolean) {
        try {
           // roundProgressBar.visibility = if (value) View.VISIBLE else View.GONE
        } catch (e: Exception) {
            //Do nothing
        }
    }

    fun addFragment(fragmentStr: String, bundle: Bundle?) {
        val fragment = FragmentFactory.fragment(fragmentStr, bundle)
        fragment?.let { baseFragment ->
            addFragmentNavigationWithBackStack(activity as BaseActivity<*>, R.id.fragmentLayout, baseFragment)
        }
    }

}