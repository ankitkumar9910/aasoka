package com.octopus_k_12.base

import android.annotation.SuppressLint
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import androidx.viewpager.widget.ViewPager
import com.network.core.BaseFragment
import com.network.core.BaseViewModel
import com.octopus_k_12.room.entities.User


abstract class BaseTabStatePagerFragment<VM : BaseViewModel> : BaseWrapperFragment<VM>() {
    private var pagerAdapter: StateViewPagerAdapter? = null

    internal inner class StateViewPagerAdapter(manager: FragmentManager) : FragmentStatePagerAdapter(manager, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {

        private val mFragmentList = ArrayList<BaseFragment<*>>()
        private val mFragmentTitleList = ArrayList<String>()

        override fun getItem(position: Int): Fragment {
            return mFragmentList.get(position)
        }

        override fun getCount(): Int {
            return mFragmentList.size
        }

        fun addFragment(fragment: BaseFragment<*>, title: String) {
            mFragmentList.add(fragment)
            mFragmentTitleList.add(title)
        }

        override fun getPageTitle(position: Int): CharSequence {
            return mFragmentTitleList[position]
        }
    }

    @SuppressLint("ClickableViewAccessibility")
    protected fun setupViewPager(viewPager: ViewPager?, accessoryCategoryList: List<User>) {
        pagerAdapter = StateViewPagerAdapter(childFragmentManager)
        pagerAdapter?.let { pageAdapter ->
            pageAdapter.apply {
                for ( category in accessoryCategoryList) {
                    val bundle = Bundle()
                  //  bundle.putString(Constants.Accessories.ACCESSORY_CATEGORY_ID,category._id )
                  //  bundle.putString(Constants.CATEGORY_NAME, category.name)
                   /* FragmentFactory.fragment(FragmentFactory.Screens.ACCESSORY_CATEGORY_PRODUCT_LIST, bundle)?.let {
                        pageAdapter.addFragment(it, category.name)
                    }*/
                }
                viewPager?.let { vp ->
                    vp.adapter = pagerAdapter
                    showRoundProgressBar(false)
                }
            }
        }
    }
}