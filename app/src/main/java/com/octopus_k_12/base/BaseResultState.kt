package com.octopus_k_12.base

sealed class BaseResultState {
    data class Success<out T>(val data: T?) : BaseResultState()
    data class Failure(val errorMessage: String, val errorCode: Int) : BaseResultState()

}
