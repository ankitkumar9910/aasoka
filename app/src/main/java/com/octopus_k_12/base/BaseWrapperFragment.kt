package com.octopus_k_12.base

import android.os.Bundle
import android.view.View
import androidx.activity.OnBackPressedDispatcher
import androidx.activity.addCallback
import com.octopus_k_12.utils.FragmentNavigation
import com.network.core.BaseActivity
import com.network.core.BaseFragment
import com.network.core.BaseViewModel
import com.network.utils.Constants
import com.google.firebase.analytics.FirebaseAnalytics
import com.octopus_k_12.R
import com.octopus_k_12.utils.*
import com.view.fragment.BottomFragment

abstract class BaseWrapperFragment <VM : BaseViewModel> : BaseFragment<VM>(), FragmentNavigation {
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setRoundProgressBar()
    }

    fun handleOnBack() {
        requireActivity().onBackPressedDispatcher.addCallback(this) {
            popBackStack()
        }
    }

    fun backPress(): OnBackPressedDispatcher {
        return requireActivity().onBackPressedDispatcher
    }

    fun addFrament(fragmentStr: String, bundle: Bundle?) {
        val fragment = FragmentFactory.fragment(fragmentStr, bundle)
        fragment?.let { baseFrament ->
            addFragmentNavigationWithBackStack(activity as BaseActivity<*>, R.id.fragmentLayout, baseFrament)
        }
    }

    fun addFrament(fragment: BaseFragment<*>?) {
        fragment?.let { baseFrament ->
            addFragmentNavigationWithBackStack(activity as BaseActivity<*>, R.id.fragmentLayout, baseFrament)
        }
    }

    fun replaceFrament(fragmentStr: String, bundle: Bundle?) {
        val fragment = FragmentFactory.fragment(fragmentStr, bundle)
        fragment?.let { baseFrament ->
            replaceToBackStack(activity as BaseActivity<*>, R.id.fragmentLayout, baseFrament)
        }
    }

    fun redirectToBottomTab(tab: Int) {
        val fragment: BaseFragment<*>? = Utils.getTopVisibleFragment(activity as BaseActivity<*>)
        fragment?.let { bottomFragment ->
            if (bottomFragment is BottomFragment) {
                (fragment as BottomFragment).setBottomTab(tab)
            }
        }
    }




    fun replaceBottomFragment() {
        replaceFrament(FragmentFactory.Screens.BOTTOM_FRAGMENT, null)
    }

    protected fun goToHome() {
        fragmentManager?.popBackStack(BottomFragment::class.java.simpleName, 0)
    }







}