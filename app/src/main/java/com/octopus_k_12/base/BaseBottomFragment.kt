package com.octopus_k_12.base

import android.annotation.SuppressLint
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.MenuItem
import android.view.View
import androidx.annotation.NonNull
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import androidx.viewpager.widget.ViewPager
import com.network.core.BaseFragment
import com.network.core.BaseViewModel
import com.network.utils.Constants
import com.network.utils.Constants.Companion.HOME_TABS
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.octopus_k_12.R
import com.octopus_k_12.utils.FirebaseAnalyticsLog
import com.octopus_k_12.utils.FirebaseEventNameENUM
import com.octopus_k_12.utils.FragmentFactory
import kotlinx.android.synthetic.main.bottom_fragment.*
import com.network.utils.Constants.Companion.ASSESSMENT_TABS
import com.network.utils.Constants.Companion.EBOOK_TABS
import com.network.utils.Constants.Companion.MORE_TABS
import com.network.utils.Constants.Companion.PROGRESS_TABS
import com.view.viewmodel.OtpVerificationViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel

abstract class BaseBottomFragment<VM : BaseViewModel>: BaseWrapperFragment<VM>() {

    private var pagerAdapter : ViewPagerAdapter? = null
    private val homeViewModel: OtpVerificationViewModel by viewModel()
    internal inner class ViewPagerAdapter(manager: FragmentManager) : FragmentPagerAdapter(manager, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT){
        private val mFragmentList = ArrayList<BaseFragment<*>>()
        private val mFragmentTitleList = ArrayList<String>()

        override fun getItem(position: Int): BaseFragment<*> {
            return mFragmentList[position]
        }

        override fun getCount(): Int {
            return mFragmentList.size
        }

        fun addFragment(fragment: BaseFragment<*>, title: String) {
            mFragmentList.add(fragment)
            mFragmentTitleList.add(title)
        }

        override fun getPageTitle(position: Int): CharSequence {
            return mFragmentTitleList[position]
        }
    }

    private fun tabChange(position : Int) {

        when (position) {
            Constants.TABS.HOME_TABS -> {
                bottomNavigationView.selectedItemId = R.id.navigation_home
            }
          /*  Constants.TABS.NCERT_TABS -> {
                bottomNavigationView.selectedItemId = R.id.navigation_ncert
            }*/
            Constants.TABS.EBOOK_TABS -> {
                bottomNavigationView.selectedItemId = R.id.navigation_ebook
            }
            Constants.TABS.ASSESSMENT_TABS -> {
                bottomNavigationView.selectedItemId = R.id.navigation_assessment
            }
            else -> {
                bottomNavigationView.selectedItemId = R.id.navigation_more
            }
        }}


    fun getViewPager() : ViewPager? {
        return viewPager
    }


    @SuppressLint("ClickableViewAccessibility")
    protected fun setupViewPager(viewPager: ViewPager?) {
        bottomNavigationView.visibility = View.VISIBLE
        pagerAdapter = ViewPagerAdapter(childFragmentManager)

        pagerAdapter?.let { adapter->

            adapter.apply {
                FragmentFactory.fragment(FragmentFactory.Screens.HOME_FRAGMENT,null)?.let {
                    adapter.addFragment(it, Constants.TABS.HOME_TAB)
                }
               /* FragmentFactory.fragment(FragmentFactory.Screens.NCERT_FRAGMENT,null)?.let {
                    adapter.addFragment(it, Constants.TABS.PROGRESS_TAB)
                }*/
                FragmentFactory.fragment(FragmentFactory.Screens.EBOOK_FRAGMENT,null)?.let {
                    adapter.addFragment(it, Constants.TABS.EBOOK_TAB)
                }
                FragmentFactory.fragment(FragmentFactory.Screens.ASSESMENT_FRAGMENT,null)?.let {
                    adapter.addFragment(it, Constants.TABS.ASSESSMENT_TAB)
                }
                FragmentFactory.fragment(FragmentFactory.Screens.MORE_FRAGMENT,null)?.let {
                    adapter.addFragment(it, Constants.TABS.MORE_TAB)
                }
                viewPager?.let { vp->
                    vp.adapter = adapter
                    vp.offscreenPageLimit = adapter.count

                    vp.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
                        override fun onPageScrollStateChanged(state: Int) {
                        }
                        override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {
                        }
                        override fun onPageSelected(position: Int) {
                            setBottomTab(position)
                        }
                    })
                }
                showRoundProgressBar(false)
            }
        }
    }


    protected val mOnNavigationItemSelectedListener = object : BottomNavigationView.OnNavigationItemSelectedListener {
        override fun onNavigationItemSelected(@NonNull item: MenuItem): Boolean {

            when (item.itemId) {
                R.id.navigation_home -> {
                    val args = Bundle()
                    args.putString(FirebaseEventNameENUM.FBE_SCREEN,HOME_TABS )
                    FirebaseAnalyticsLog.trackFireBaseEventLog(FirebaseEventNameENUM.FBE_BOTTOM_TAB,args)
                    getViewPager()?.setCurrentItem(Constants.TABS.HOME_TABS,true)
                    return true
                }
             /*   R.id.navigation_ncert -> {
                    val args = Bundle()
                    args.putString(FirebaseEventNameENUM.FBE_SCREEN,PROGRESS_TABS  )
                    FirebaseAnalyticsLog.trackFireBaseEventLog(FirebaseEventNameENUM.FBE_BOTTOM_TAB,args)
                    getViewPager()?.setCurrentItem(Constants.TABS.NCERT_TABS,true)
                    return true
                }*/
                R.id.navigation_ebook -> {
                    val args = Bundle()
                    args.putString(FirebaseEventNameENUM.FBE_SCREEN, EBOOK_TABS )
                    FirebaseAnalyticsLog.trackFireBaseEventLog(FirebaseEventNameENUM.FBE_BOTTOM_TAB,args)
                    getViewPager()?.setCurrentItem(Constants.TABS.EBOOK_TABS,true)
                    return true
                }
                R.id.navigation_assessment -> {
                    val args = Bundle()
                    args.putString(FirebaseEventNameENUM.FBE_SCREEN, ASSESSMENT_TABS )
                    FirebaseAnalyticsLog.trackFireBaseEventLog(FirebaseEventNameENUM.FBE_BOTTOM_TAB,args)
                    getViewPager()?.setCurrentItem(Constants.TABS.ASSESSMENT_TABS,true)
                    return true
                }
                R.id.navigation_more -> {
                    val args = Bundle()
                    args.putString(FirebaseEventNameENUM.FBE_SCREEN, MORE_TABS )
                    FirebaseAnalyticsLog.trackFireBaseEventLog(FirebaseEventNameENUM.FBE_BOTTOM_TAB,args)
                    getViewPager()?.setCurrentItem(Constants.TABS.MORE_TABS,true)
                    return true
                }
            }
            return false
        }
    }

    fun setBottomTab(position : Int) {
        tabChange(position)
        getViewPager()?.setCurrentItem(position,true)
    }

    fun setDeepLinkTab(catId : Int,catName:String) {
        Handler(Looper.getMainLooper()).postDelayed({
            val bundle = Bundle()
            bundle.putString(Constants.CAT_TAB_ID, catId.toString())
            bundle.putString(Constants.CATEGORY_NAME, catName)
          //  addFrament(FragmentFactory.Screens.SERVICES_LIST_FRAGMENT,bundle)
        }, 500)

    }


}