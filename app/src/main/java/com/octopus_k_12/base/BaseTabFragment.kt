package com.octopus_k_12.base

import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.network.core.BaseFragment
import com.network.core.BaseViewModel

abstract class BaseTabFragment<VM : BaseViewModel> : BaseWrapperFragment<VM>() {
    private var pagerAdapter: ViewPagerAdapter? = null

    internal inner class ViewPagerAdapter(manager: FragmentManager) : FragmentPagerAdapter(manager, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {
        private val mFragmentList = ArrayList<BaseFragment<*>>()
        private val mFragmentTitleList = ArrayList<String>()
        override fun getItem(position: Int): BaseFragment<*> {
            return mFragmentList[position]
        }

        override fun getCount(): Int {
            return mFragmentList.size
        }

        fun addFragment(fragment: BaseFragment<*>, title: String) {
            mFragmentList.add(fragment)
            mFragmentTitleList.add(title)
        }

        override fun getPageTitle(position: Int): CharSequence {
            return mFragmentTitleList[position]
        }
    }
/*

    @SuppressLint("ClickableViewAccessibility")
    protected fun setupViewPager(viewPager: ViewPager?, homeData: List<HomeCategory>, catTabId: Int) {
        pagerAdapter = ViewPagerAdapter(childFragmentManager)
        var position = 0
        pagerAdapter?.let { pageAdapter ->
            pageAdapter.apply {
                for ((index, homeCat) in homeData.withIndex()) {
                    if (homeCat.id == catTabId) {
                        position = index
                    }
                    val bundle = Bundle()
                    bundle.putString(Constants.CATEGORY_ID, homeCat.id.toString())
                    bundle.putString(Constants.CATEGORY_NAME, homeCat.name)
                   */
/* FragmentFactory.fragment(FragmentFactory.Screens.SERVICES_LIST_FRAGMENT, bundle)?.let {
                        pageAdapter.addFragment(it, homeCat.name!!)
                    }*//*

                }
                viewPager?.let { vp ->
                    vp.adapter = pagerAdapter
                    Handler(Looper.getMainLooper()).postDelayed({
                        vp.setCurrentItem(position, true)
                    }, 50)
                    showRoundProgressBar(false)
                }
            }
        }
    }
*/
}