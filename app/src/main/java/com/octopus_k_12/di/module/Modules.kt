package com.octopus_k_12.di.module

import android.annotation.SuppressLint
import android.app.Application
import android.provider.Settings
import androidx.room.Room
import com.octopus_k_12.App
import com.octopus_k_12.BuildConfig
import com.octopus_k_12.R
import com.octopus_k_12.room.AppDatabase
import com.octopus_k_12.room.RoomModule
import com.octopus_k_12.room.repository.*
import com.network.retrofit.retroApi.*
import com.view.viewmodel.*
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

object Modules {
    private val applicationModules = module {
        single {
            get<Application>().run {
                getSharedPreferences(getString(R.string.app_name), 0)
            }
        }
        single {
            Room.databaseBuilder(get(), AppDatabase::class.java, BuildConfig.DEFAULT_ROOM).allowMainThreadQueries().fallbackToDestructiveMigration().build()
        }
    }
    private val viewModelModules = module {
        viewModel { SignUpViewModel(get(), get()) }
           viewModel { OtpVerificationViewModel(get(), get(), get()) }
           viewModel { BottomViewModel(get(), get()) }
           viewModel { HomeViewModel(get(), get()) }

    }
    private val networkModules = module {
        single { NetworkBuilder.create(NetworkBuilder.BASE_URL, SendOtpApi::class.java, get(), androidID) }
        single { NetworkBuilder.create(NetworkBuilder.BASE_URL, VerifyLoginApi::class.java, get(), androidID) }
        single { NetworkBuilder.create(NetworkBuilder.BASE_URL, Api::class.java, get(), androidID) }

           }

    private val repoModules = module {
        single<LoginRepository> {
            LoginRepositoryImpl(
                get(),
                get()
            )
        }
        single<OtpRepository> {
            OtpRepositoryImpl(
                get(),
                get()
            )
        }
         single<UserRepository> { UserRepositoryImpl(get()) }
        single<ApiRepository> { ApiRepositoryImpl(get(), get(), get()) }

         }
    private val daoModules = module {
        single { RoomModule(get()).getUserDao() }

        single { RoomModule(get()).getDBQuery() }
    }

    @SuppressLint("HardwareIds")
    var androidID = Settings.Secure.getString(App.applicationContext().contentResolver, Settings.Secure.ANDROID_ID)?:""

    fun getAll() = listOf(applicationModules, viewModelModules, networkModules, repoModules, daoModules)

}