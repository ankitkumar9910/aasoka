package com.octopus_k_12

import android.content.Context
import androidx.multidex.MultiDexApplication
import com.network.utils.Constants
import com.octopus_k_12.di.module.Modules

import com.octopus_k_12.utils.FirebaseAnalyticsLog
import com.octopus_k_12.utils.HttpsTrustManager
import org.koin.android.ext.koin.androidContext

class App : MultiDexApplication() {

    init {
        instance = this
    }

    companion object {
        private var instance: App? = null

        fun applicationContext() : Context {
            return instance!!.applicationContext
        }
    }

    override fun onCreate() {
        super.onCreate()


        startKoin()
        FirebaseAnalyticsLog.init(this)

        // moengage

    }

    private fun startKoin(){
        org.koin.core.context.startKoin {
            androidContext(this@App)
            modules(Modules.getAll())
        }
    }

    fun restartKoin(){
        org.koin.core.context.stopKoin()
        startKoin()
    }



}