package com.octopus_k_12.utils

import android.os.Build
import android.text.Html
import android.text.Spanned

class HtmlCompat {
    companion object {
        fun fromHtml(source: String): Spanned =
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                Html.fromHtml(source, Html.FROM_HTML_MODE_LEGACY)
            } else {
                Html.fromHtml(source)
            }
    }
}
