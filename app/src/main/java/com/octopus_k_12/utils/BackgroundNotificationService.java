package com.octopus_k_12.utils;

import android.app.IntentService;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Intent;
import android.media.AudioAttributes;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import androidx.core.app.NotificationCompat;


import com.network.retrofit.retroApi.RetrofitInterface;
import com.network.utils.Constants;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import okhttp3.ResponseBody;
import retrofit2.Retrofit;

public class BackgroundNotificationService extends IntentService {
    String BaseUrl = "";
    String file_id = "";
    String file_name = "";
    String type="";
    boolean view=false;
    private NotificationCompat.Builder notificationBuilder;
    private NotificationManager notificationManager;
    String url = "";

    public BackgroundNotificationService() {
        super("Service");
    }

    /* access modifiers changed from: protected */
    public void onHandleIntent(Intent intent) {
        if (intent.getExtras() != null) {
            Bundle bundle = intent.getExtras();
            file_name = bundle.getString(Constants.IMAGE_NAME);
            file_id = bundle.getString(Constants.IMAGE_ID);
          BaseUrl = bundle.getString(Constants.IMAGE_BASE_URL);
            type = bundle.getString(Constants.IMAGE_TYPE);
            view=bundle.getBoolean(Constants.IMAGE_VIEW);
            this.url = bundle.getString(Constants.IMAGE_URL);
        }
        this.notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        if (Build.VERSION.SDK_INT >= 26) {
            NotificationChannel notificationChannel = new NotificationChannel("id", "an", NotificationManager.IMPORTANCE_HIGH);
            notificationChannel.setDescription("no sound");
            notificationChannel.setSound((Uri) null, (AudioAttributes) null);
            notificationChannel.enableLights(false);
            notificationChannel.setLightColor(-16776961);
            notificationChannel.enableVibration(false);
            notificationChannel.setLockscreenVisibility(1);
            this.notificationManager.createNotificationChannel(notificationChannel);
        }
        NotificationCompat.Builder autoCancel = new NotificationCompat.Builder(this, "id").setSmallIcon(17301633).setContentTitle(file_name).setDefaults(0).setAutoCancel(true);
        this.notificationBuilder = autoCancel;
        if(!view){
        this.notificationManager.notify(0, autoCancel.build());}
        initRetrofit();
    }

    private void initRetrofit() {
        try {
            downloadImage(((RetrofitInterface) new Retrofit.Builder().baseUrl(BaseUrl).build().create(RetrofitInterface.class)).downloadFile(this.url).execute().body());
        } catch (IOException e) {
            e.printStackTrace();
            Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_LONG).show();
        }
    }

    private void downloadImage(ResponseBody body) throws IOException {
        try {
            byte[] data = new byte[307200];
            long fileSize = body.contentLength();
            InputStream inputStream = new BufferedInputStream(body.byteStream(), 614400);
            File outputFile = new File(getFilesDir(), this.file_name);
            if (!outputFile.exists()) {
                outputFile.createNewFile();
            }
            OutputStream outputStream = new FileOutputStream(outputFile);
            long total = 0;
            boolean downloadComplete = false;
            while (true) {
                int read = inputStream.read(data);
                int count = read;
                if (read != -1) {
                    total += (long) count;
                    double d = (double) (100 * total);
                    double d2 = (double) fileSize;
                    Double.isNaN(d);
                    Double.isNaN(d2);

                    updateNotification((int) (d / d2));
                    outputStream.write(data, 0, count);
                    downloadComplete = true;
                } else {
                    onDownloadComplete(downloadComplete);
                    outputStream.flush();
                    outputStream.close();
                    inputStream.close();
                    return;
                }
            }
        } catch (Exception e) {
            Log.e("download error", e.toString());
        }
    }

    private void updateNotification(int currentProgress) {
        this.notificationBuilder.setProgress(100, currentProgress, false);
        NotificationCompat.Builder builder = this.notificationBuilder;
        builder.setContentText("Downloaded: " + currentProgress + "%");
        if(!view){
        this.notificationManager.notify(0, this.notificationBuilder.build());}
        Intent intent = new Intent("progress_update");
        intent.putExtra("progress", currentProgress);
        sendBroadcast(intent);
    }

    private void sendProgressUpdate(boolean downloadComplete) {
        Intent intent = new Intent("progress_update");
        intent.putExtra("downloadComplete", downloadComplete);
        sendBroadcast(intent);
    }

    private void onDownloadComplete(boolean downloadComplete) {

        this.notificationManager.cancel(0);
        this.notificationBuilder.setProgress(0, 0, false);
        this.notificationBuilder.setContentText(" Download Complete");
        if(!view){
        this.notificationManager.notify(0, this.notificationBuilder.build());}
    }

    public void onTaskRemoved(Intent rootIntent) {
        this.notificationManager.cancel(0);
    }
}
