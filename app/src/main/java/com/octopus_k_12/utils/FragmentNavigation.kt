package com.octopus_k_12.utils

import androidx.annotation.IdRes
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.fragment.app.FragmentTransaction
import androidx.fragment.app.transaction
import com.network.core.BaseBottomSheetDialogFragment
import com.network.core.BaseDialogFragment
import com.network.core.BaseFragment
import com.crashlytics.android.Crashlytics

interface FragmentNavigation {

    fun addFragmentNavigationWithBackStack(
        activity: FragmentActivity, @IdRes contentId: Int, newFragment: BaseFragment<*>) {
        val destinationFragment: Fragment?
        with(activity.supportFragmentManager) {
            destinationFragment = findFragmentByTag(newFragment.fragmentTag())
        }
        val ft: FragmentTransaction = activity.supportFragmentManager.beginTransaction()
        ft.setCustomAnimations(android.R.animator.fade_in, android.R.animator.fade_out)

        destinationFragment?.let {
            ft.attach(destinationFragment)
        } ?: run {
            ft.add(contentId, newFragment)
            ft.addToBackStack(newFragment.javaClass.name)
        }

        ft.commitAllowingStateLoss()
    }

    fun showDialog(activity: FragmentActivity, newFragment: BaseDialogFragment<*>) {
        newFragment.show(activity.supportFragmentManager, newFragment.fragmentTag())
    }

    fun showBottomSheetDialog(activity: FragmentActivity, newFragment: BaseBottomSheetDialogFragment<*>) {
        newFragment.show(activity.supportFragmentManager, newFragment.fragmentTag())
    }

    fun isFragmentVisible(activity: FragmentActivity, tag: String): Boolean {
        return activity.supportFragmentManager.findFragmentByTag(tag)?.isVisible ?: false
    }

    fun resetFragmentNavigation(activity: FragmentActivity) {
        activity.supportFragmentManager.transaction {
            for (fragment in activity.supportFragmentManager.fragments) {
                detach(fragment)
                remove(fragment)
            }
        }
    }

    fun replaceToBackStack(activity: FragmentActivity, @IdRes contentId: Int, fragment: BaseFragment<*>) {
        clearBackStackreplace(activity)
        activity.supportFragmentManager
            .beginTransaction()
            .replace(contentId, fragment)
            .addToBackStack(fragment.fragmentTag()).commitAllowingStateLoss()
    }
    fun clearBackStackreplace(activity: FragmentActivity) {
        try {
            val fm = activity.supportFragmentManager
            val count = fm.backStackEntryCount
            for (i in 0 until count) {
                fm.popBackStackImmediate()
            }
        } catch (e: Exception) {
            e.printStackTrace()
            Crashlytics.logException(e)
        }
    }

    fun clearBackStack(activity: FragmentActivity) {
        try {
            val fm = activity.supportFragmentManager
            val count = fm.backStackEntryCount
            for (i in 1 until count) {

                fm.popBackStackImmediate()
            }
        } catch (e: Exception) {
            e.printStackTrace()
            Crashlytics.logException(e)
        }
    }
}