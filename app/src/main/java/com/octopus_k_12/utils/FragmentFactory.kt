package com.octopus_k_12.utils

import android.os.Bundle
import com.network.core.BaseFragment

import com.view.fragment.*


class FragmentFactory {
	abstract class Screens private constructor() {
		init {
			throw IllegalStateException("Screens class")
		}

		companion object {
			const val HOME_FRAGMENT = "homeFragment"
			const val PROGRESS_FRAGMENT = "progressFragment"
			const val EBOOK_FRAGMENT = "ebookFragment"
			const val ASSESMENT_FRAGMENT = "assessmentFragment"
			const val MORE_FRAGMENT = "moreFragment"
			const val SIGNUP_FRAGMENT = "signUpFragment"
			const val USER_REGISTRATION_FRAGMENT = "userregistrationFragment"
			const val BOTTOM_FRAGMENT = "bottomFragment"
			const val OTP_FRAGMENT = "otpFragment"
			const val SUBJECT_FRAGMENT = "SubjectFragment"
			const val LIVE_CLASS_FRAGMENT = "LiveClassFragment"
			const val CONTENT_FRAGMENT = "ContentFragment"
			const val ASSIGNMENT_FRAGMENT = "AssignmentFragment"
			const val ONBOARDING_FRAGMENT = "OnBoardingScreen"
			const val SUBSCRIPTION_FRAGMENT = "SubscriptionFragment"

		}
	}

	companion object  {
		/**
		 * Returns fragment either from not or base app
		 */
		fun fragment(fragmentName :String?, params :Bundle?) : BaseFragment<*>? {
			var paramBundle = params
			var contentFragment : BaseFragment<*>? = null
			if(fragmentName != null && fragmentName.isNotEmpty()) {
				if(paramBundle == null) {
					paramBundle = Bundle()
				}

					contentFragment = getFragmentFromAppPackage(fragmentName)

				contentFragment?.arguments = paramBundle
			}
			return contentFragment
		}

		/**7
		 * Returns fragment from base application
		 */
		private fun getFragmentFromAppPackage(fragment :String) : BaseFragment<*>? {
			var contentFragment : BaseFragment<*>? = null
			when(fragment) {
				Screens.HOME_FRAGMENT -> contentFragment = HomeFragment()
				Screens.PROGRESS_FRAGMENT -> contentFragment = ProgresFragment()
				Screens.EBOOK_FRAGMENT -> contentFragment = ProgresFragment()
				Screens.ASSESMENT_FRAGMENT -> contentFragment =  ProgresFragment()
				Screens.MORE_FRAGMENT -> contentFragment = MoreFragment()
				Screens.SIGNUP_FRAGMENT -> contentFragment = SignUpFragment()
				Screens.USER_REGISTRATION_FRAGMENT -> contentFragment =  ProgresFragment()
				Screens.BOTTOM_FRAGMENT -> contentFragment = BottomFragment()
				Screens.OTP_FRAGMENT -> contentFragment = OtpFragment()

				Screens.SUBJECT_FRAGMENT -> contentFragment =  ProgresFragment()


				Screens.ASSIGNMENT_FRAGMENT -> contentFragment =  ProgresFragment()

				Screens.ONBOARDING_FRAGMENT -> contentFragment = OnBoardingFragment()

			}
			return contentFragment
		}

		/**
		 * Returns fragment from base application
		 */

		/**
		 * Returns fragment either from not or base app
		 */

	}
}