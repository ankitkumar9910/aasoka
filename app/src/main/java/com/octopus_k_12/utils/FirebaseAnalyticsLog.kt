package com.octopus_k_12.utils

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import com.network.utils.Constants
import com.google.firebase.analytics.FirebaseAnalytics

import com.octopus_k_12.R
class FirebaseAnalyticsLog {

    companion object {
        private var firebaseAnalytics: FirebaseAnalytics? = null

        private var shared: SharedPreferences? = null
        fun init(context: Context) {
            firebaseAnalytics = FirebaseAnalytics.getInstance(context)
            shared = context.getSharedPreferences(context.getString(R.string.app_name), 0)

        }


        fun trackFireBaseEventLog(eventName: String, keyBundle: Bundle) {

            firebaseAnalytics?.logEvent(eventName, keyBundle)

        }

    }

}
