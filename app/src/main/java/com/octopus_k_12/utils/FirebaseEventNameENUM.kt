package com.octopus_k_12.utils

class FirebaseEventNameENUM {
    companion object {

        const val FBE_BOTTOM_TAB: String = "bottom_tab"
        const val FBE_NOTIFICATION_CLICK = "firebase_notification_click"
        const val FBE_NOTIFICATION_TITLE = "firebase_notification_title"
        const val FBE_SCREEN = "fire_screen"
        const val FBE_USER_LOGIN = "user_login"
        const val FBE_SELCT_SUBJECT = "select_subject"
        const val FBE_SELCT_LIVECLASS = "select_live_class"
        const val FBE_SELECT_CHAPTER = "select_chapter"
        const val FBE_SELECT_RECOMMENDED = "select_recommended"
        const val FBE_SELECT_PLAY_CONTENT = "play_Content"
        const val FBE_EXIT_CONTENT = "exit_content"
        const val FBE_CREATE_ASSESSMENT = "create_assessment"
        const val FBE_ATTEMP_ASSESSMENT = "attemp_assessment"
        const val FBE_SELECT_EBOOK = "select_ebook"
        const val FBE_CREATE_PRACTICE_ASSESSMENT = "create_practice_assessment"
        const val FBE_MOBILE_NUMBER = "mobileNumber"
        const val FBE_OLD_USER_ID = "OldUserId"
        const val FBE_USER_ID = "userId"
        const val FBE_SUBJECT_ID = "SubjectId"
        const val FBE_SUBJECT_NAME = "subjectName"
        const val FBE_CLASS_ID = "classId"
        const val FBE_LIVE_CLASS_ID = "liveClassId"
        const val FBE_LIVE_CLASS_NAME = "liveClassName"
        const val FBE_LIVE_CLASS_MEETING_CODE = "liveClassMeetingCode"
        const val FBE_CHAPTER_ID = "ChapterId"
        const val FBE_CHAPTER_NAME = "ChapterName"
        const val FBE_CONTENT_ID = "ContentId"
        const val FBE_CONTENT_NAME = "contentName"
        const val FBE_CONTENT_URL = "ContentUrl"
        const val FBE_PLAY_TIME = "playtime"
        const val FBE_ASSESSMENT_NAME = "assessmentName"
        const val FBE_ASSESSMENT_ID = "assessmentName"
        const val FBE_EBOOK_NAMEFBE_ASSESSMENT_ID = "assessmentId"
        const val FBE_EBOOK_NAME = "ebookName"
        const val FBE_EBOOK_ID = "ebookId"
        const val FBE_EBOOK_URL = "ebookUrl"

        const val FBE_INIT_LOGIN = "init_login"

    }
}