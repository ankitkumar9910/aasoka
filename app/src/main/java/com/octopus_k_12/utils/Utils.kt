package com.octopus_k_12.utils

import android.content.Context
import android.content.SharedPreferences
import android.widget.Toast
import androidx.fragment.app.FragmentActivity
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import com.network.core.BaseFragment
import com.network.utils.Constants
import com.octopus_k_12.R
import java.text.DecimalFormat
import java.text.SimpleDateFormat
import java.util.*
import java.util.regex.Pattern

class Utils {
    companion object {
        fun numberToCurrencyString(amount: Double): String {
            val amounts: String
            return try {
                val formatter = DecimalFormat(Constants.WITH_DECIMAL_FORMAT)
                amounts = formatter.format(amount.toInt())
                if (amounts.contains(".00")) {
                    amounts.split("\\.".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()[0]
                } else {
                    amounts
                }
            } catch (e: Exception) {
                "" + amount
            }
        }
fun getOldUserId(context: Context):String {
    var sharedPreferences=context.getSharedPreferences("Octopus_user",0)
    return  sharedPreferences.getString("user_id","")?:""
}
        fun firstCapWordConveter(data:String):String{
            return try {
                val strArrayOBJ = data.split(" ".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
                val builder = StringBuilder()
                for (s in strArrayOBJ) {
                    val cap = s.substring(0, 1).toUpperCase(Locale.getDefault()) + s.substring(1)
                    builder.append("$cap ")
                }
                builder.toString()
            } catch (e:Exception){
                data
            }
        }
        fun convertVideoLength(videoDuration: Long): Long {
            var seconds:Long = 0
            try {
                val duration: Long = videoDuration / 1000

                 seconds = duration

            } catch (e: Exception) {
                e.printStackTrace()
            }
            return seconds
        }

        fun getSubscriptionDateFormated(subscriptionDate: String?): String {
            return try {
                val placedDate = subscriptionDate?.toLong()?.let { Date(it) }
                val formatter1 = SimpleDateFormat("yyyy-MM-dd hh:mm:ss", Locale.getDefault())
                val placingDate = formatter1.format(placedDate?:Date())
                //Date coming from the server is in UTC timezone
                formatter1.timeZone = TimeZone.getTimeZone("UTC")
                //We need to convert this date to whatever timezone the device is currently in
                val formatter2 = SimpleDateFormat("dd MMMM, yyyy", Locale.getDefault())
                formatter2.timeZone = TimeZone.getDefault()

                //Return the formatted date
                formatter2.format(formatter1.parse(placingDate)?:Date())
            } catch (e: Exception) {
                ""
            }
        }


        fun getDateTimeFormated(dateStr: String?): String {
            var orderDate = dateStr?:""
            try {
                val formatter1 = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())
                val formatter2 = SimpleDateFormat("EEE, d MMM yyyy", Locale.getDefault())

                orderDate = formatter2.format(formatter1.parse(dateStr?.split("T")?.get(0)?:"")?:"")
            } catch (e: Exception) {
                e.printStackTrace()
            }

            return orderDate
        }

        fun checkSpecialCharacters(s: String): Boolean {
            val pattern = Pattern.compile("[\$&+,:;=\\\\?@#|/'<>.^*()%!-]")
            return pattern.matcher(s).find()
        }

        fun isValidRegNo(s: String): Boolean {
            val pattern = Pattern.compile("^[a-zA-Z0-9]{5,10}$")
            return pattern.matcher(s).matches()
        }

        fun showToast(act: Context, msg: String) {
//            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
//                val toast = Toast.makeText(act, msg, Toast.LENGTH_LONG)
//                val view = toast.view
//                view?.setBackgroundResource(R.drawable.rectangular_bg_gray)
//                val text = view?.findViewById(android.R.id.message) as TextView
//                text.setTextColor(ContextCompat.getColor(act ,R.color.white))
//                text.setPadding(10,5,10,5)
//                toast.show()
//            } else {
                Toast.makeText(act, msg, Toast.LENGTH_LONG).show()
//            }
        }

        fun epocData(str: String): String {
            return try {
                SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH).format(java.sql.Date(str.toLong()))
            } catch (e: Exception) {
                ""
            }
        }

        fun calculateTime(startDate: Date, endDate: Date): Long {
            return try {
                endDate.time - startDate.time
            } catch (e: Exception) {
                0
            }
        }

        fun getTopVisibleFragment(context: FragmentActivity): BaseFragment<*> {
            return context.supportFragmentManager.findFragmentById(R.id.fragmentLayout) as BaseFragment<*>
        }

        inline fun <reified T> genericCastOrNull(anything: Any): T? {
            return anything as? T
        }




        private fun getCurrentHour(): String? {
            val cal = Calendar.getInstance()
            val sdfHour = SimpleDateFormat("HH:mm", Locale.getDefault())
            return sdfHour.format(cal.time)
        }

        /**
         * @param  target  hour to check
         * @param  start   interval start
         * @param  end     interval end
         * @return true    true if the given hour is between
         */
        private fun isHourInInterval(target: String?, start: String?, end: String?): Boolean {
            return target?.compareTo(start?:"")?:0 >= 0 && target?.compareTo(end?:"")?:0 <= 0
        }

        /**
         * @param  start   interval start
         * @param  end     interval end
         * @return true    true if the current hour is between
         */
        fun isNowInInterval(start: String?, end: String?): Boolean {
            return isHourInInterval(getCurrentHour(), start, end)
        }


    }
}