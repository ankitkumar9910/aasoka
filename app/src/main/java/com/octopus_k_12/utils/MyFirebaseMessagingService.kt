package com.octopus_k_12.utils

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.media.RingtoneManager
import android.net.Uri
import android.os.Build
import android.os.Bundle
import androidx.core.app.NotificationCompat
import androidx.core.content.ContextCompat
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.network.utils.Constants
import com.network.utils.Constants.Companion.COME_FROM
import com.network.utils.Constants.Companion.COME_FROM_NOTIFICATION
import com.network.utils.Constants.Companion.CUSTOM_NOTIFICATION_URL
import com.network.utils.Constants.Companion.NOTIFICATION_TYPE
import com.network.utils.Constants.Companion.NOTIFICATION_TYPE_ORDER_STATUS
import com.network.utils.Constants.Companion.ORDER_ID
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import com.octopus_k_12.R
import com.view.activity.HomeActivity

class MyFirebaseMessagingService : FirebaseMessagingService() { private var broadcaster: LocalBroadcastManager? = null

    override fun onCreate() {
        broadcaster = LocalBroadcastManager.getInstance(this)
    }

    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        remoteMessage.let {

          {
                var notificationData: Map<String, String>?
                // Check if message contains a data payload.
                remoteMessage.data.isNotEmpty().let {
                    notificationData = remoteMessage.data
                }

                if (notificationData?.get("action").equals("python")) {
                    val bundle = Bundle()
                    val data = remoteMessage.data
                    for ((key, value) in data) {
                        bundle.putString(key, value)
                    }
                    val notificationBroadcast: Intent





                } else {
                    // Check if message contains a notification payload.
                    remoteMessage.notification?.let {
                        if (notificationData!!.containsKey("action")) {
                            val retail = notificationData?.get("action")
                            if (retail.equals("retail")) {
                                sendNotification(it, notificationData)
                            }
                        }
                    } ?: run {
                        sendNotification(notificationData)
                    }
                }
            }
        }
    }

    override fun onNewToken(token: String) {
        //PushManager.getInstance().refreshToken(applicationContext, token)
    }

    private fun sendNotification(message: RemoteMessage.Notification, notificationData: Map<String, String>?) {
        val intent = Intent(this, HomeActivity::class.java).apply {
            putExtra(COME_FROM, COME_FROM_NOTIFICATION)
            message.clickAction?.let {
                putExtra(NOTIFICATION_TYPE, it)
            }
        }
        notificationData?.isNotEmpty()?.run {
            if (message.clickAction == NOTIFICATION_TYPE_ORDER_STATUS) {
                //  val orderId = notificationData["order_id"]
                //  intent.putExtra(ORDER_ID, orderId)
                val deepLinkURL = notificationData["deeplinkurl"]
                intent.putExtra("branch",deepLinkURL)
                intent.putExtra("branch_force_new_session", true)
                val linkURL = Uri.parse(deepLinkURL)
                if (linkURL?.authority.equals("gomechanic.in")) {
                    if (linkURL.pathSegments[0] == "order") {
                        if (linkURL.pathSegments[1].isNotEmpty()) {
                            val orderId = linkURL.pathSegments[1]
                            intent.putExtra(ORDER_ID, orderId)
                        }
                    } else if (linkURL.pathSegments[0] == "show-all-cart") {
                        val url = linkURL.pathSegments[0]
                        intent.putExtra(CUSTOM_NOTIFICATION_URL, url)
                    }
                }
            }
        }
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        val pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT)
        val channelId = getString(R.string.default_notification_channel_id)
        val defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
        val notificationBuilder = NotificationCompat.Builder(this, channelId).setBadgeIconType(NotificationCompat.BADGE_ICON_SMALL).setContentTitle(message.title).setContentText(message.body).setAutoCancel(true).setSound(defaultSoundUri).setContentIntent(pendingIntent)
        notificationBuilder?.setSmallIcon(R.drawable.ic_bottom_tab_car)
        notificationBuilder?.color = ContextCompat.getColor(this, R.color.colorRed)
        val notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        // Since android Oreo notification channel is needed.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val channel = NotificationChannel(channelId, "Channel human readable title", NotificationManager.IMPORTANCE_DEFAULT)
            notificationManager.createNotificationChannel(channel)
        }
        notificationManager.notify(0 /* ID of notification */, notificationBuilder.build())
        val bundle = Bundle()
        bundle.putString(FirebaseEventNameENUM.FBE_NOTIFICATION_TITLE,message.title)
        FirebaseAnalyticsLog.trackFireBaseEventLog(FirebaseEventNameENUM.FBE_NOTIFICATION_CLICK,bundle)
    }

    private fun sendNotification(notificationData: Map<String, String>?) {
        notificationData?.let { mapData ->
            val title = mapData["title"]?:""
            val url = mapData["url"]
            val body = mapData["body"]?:""
            val intent = Intent(this, HomeActivity::class.java)
            intent.putExtra(CUSTOM_NOTIFICATION_URL, url)
            intent.putExtra("branch", url)
            intent.putExtra("branch_force_new_session", true)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            val pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent, PendingIntent.FLAG_ONE_SHOT)
            val channelId = getString(R.string.default_notification_channel_id)
            val defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
            val notificationBuilder = NotificationCompat.Builder(this, channelId)
                .setBadgeIconType(NotificationCompat.BADGE_ICON_SMALL)
                .setContentTitle(title)
                .setContentText(body)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent)
                .setStyle(NotificationCompat.BigTextStyle().bigText(body))
            notificationBuilder?.setSmallIcon(R.drawable.ic_bottom_tab_car)
            notificationBuilder?.color = ContextCompat.getColor(this, R.color.colorRed)
            val notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            // Since android Oreo notification channel is needed.
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                val channel = NotificationChannel(channelId, "Channel human readable title", NotificationManager.IMPORTANCE_DEFAULT)
                notificationManager.createNotificationChannel(channel)
            }
            notificationManager.notify(0 , notificationBuilder.build())
            val bundle = Bundle()
            bundle.putString(FirebaseEventNameENUM.FBE_NOTIFICATION_TITLE, title)
            FirebaseAnalyticsLog.trackFireBaseEventLog(FirebaseEventNameENUM.FBE_NOTIFICATION_CLICK, bundle)
        }
    }
}