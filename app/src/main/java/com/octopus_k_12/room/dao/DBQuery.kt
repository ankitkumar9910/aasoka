package com.octopus_k_12.room.dao

import androidx.room.*

@Dao
interface DBQuery {
    /*
     *  Cart Query
     */
   /* @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun addCartData(cart: Cart): Long

    @Update
    fun updateCartData(cart: Cart)

    @Query("UPDATE cart SET remark = :remark WHERE id = :cartid")
    fun updateRemarkData(cartid: Int,remark:String)

    @Query("UPDATE services_details SET strike_through_price = :strike_through_price,totalPrice=:totalPrice,isAddToCart=:isAddToCart  WHERE id = :id and retail_service_type_id=:serviceTypeId")
    fun updateServiceDetailsPrice(strike_through_price: Double,totalPrice:String,id: String,serviceTypeId:String,isAddToCart: Int)

    *//* @Query("UPDATE tyre_details SET count = :count  WHERE id = :id ")
     fun updateCount(count: Int,id: String)*//*

    *//* @Query("SELECT count FROM tyre_details where id=:id")
     fun getCount(id: String) : String*//*

    @Transaction
    @Query("SELECT cart.*,(SELECT sum(totalAmount) FROM cart_services) as totalAmount FROM (SELECT * FROM cart WHERE id=:cartId) as cart")
    fun getCartData(cartId: Int): CartRequest?

    @Query("SELECT * FROM homeadditional limit 1")
    fun getLiveSubscriptionData(): LiveData<HomeAdditional>

    @Query("SELECT * FROM cart WHERE id=:cartId")
    fun getLiveCartData(cartId: Int): LiveData<Cart>

    @Query("DELETE FROM cart")
    fun clearCart()

    @Query("DELETE FROM cart_services where parentId=:parentId")
    fun clearCartParentId(parentId: String)

    @Query("DELETE FROM cart where servicemodel LIKE  :parentId")
    fun clearCartDiscountParentId(parentId: String)

    @Transaction
    @Query("SELECT cart.*,(SELECT sum(totalAmount) FROM cart_services WHERE serviceCartId!=:serviceCartId) as totalAmount FROM (SELECT * FROM cart WHERE id=:cartId) as cart")
    fun getCartDataExcept(cartId: Int, serviceCartId: String): CartRequest?

    *//*
     *  Home page Query
     *//*
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun addHomeAdditional(user: HomeAdditional)

    @Query("SELECT * FROM homeadditional")
    fun getHomeAdditional(): HomeAdditional

    @Query("DELETE FROM homeadditional")
    fun clearHomeAdditional()

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun addGlobalSearch(cart: GlobalSearchModel): Long

    @Query("DELETE FROM  global_search where num in (select num from global_search order by num asc limit 1)")
    fun clearFirstGlobalSearchItem()
    @Query("DELETE FROM  global_search where name =:name")
    fun deleteGlobalSearch(name:String)
    @Query("SELECT * FROM global_search")
    fun getGlobalSearchHistory(): List<GlobalSearchModel>?
    *//*
     *  Cart Query
     *//*

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun addCartService(cartServices: CartServices)
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun addCartServiceList(cartServices: List<CartServices>)
    @Query("DELETE FROM cart_services WHERE parentId=:parentId AND serviceId=:serviceId")
    fun deleteCartService(parentId: String, serviceId: String)

    @Query("SELECT * FROM cart_services WHERE parentId=:parentId AND serviceId=:serviceId")
    fun getCartService(parentId: String, serviceId: String): CartServices

    @Query("SELECT serviceId,parentId,amount as totalAmount,name as title,`desc`,oil_brand,subscriptiondetails,type FROM cart_services where type!='${Constants.TYRE_TYPE}' and type!='${Constants.BATTERY_TYPE}'")
    fun getAllCartServices(): MutableList<ServiceIdModel>
    @Query("SELECT serviceId as id,parentId,name,amount as totalAmount,count,type  FROM cart_services where type='${Constants.TYRE_TYPE}' or type='${Constants.BATTERY_TYPE}'")
    fun getAllTyreCartServices(): MutableList<TyreIdModel>
    @Query("SELECT serviceCartId, serviceId,parentId,totalAmount,title,`desc`,subscriptiondetails FROM cart_services WHERE serviceCartId!=:serviceCartId")
    fun getAllCartServicesExcept(serviceCartId: String): List<ServiceIdModel>

    @Query("SELECT serviceCartId FROM cart_services WHERE parentId=:parentId AND serviceId=:serviceId")
    fun isAddedToCart(parentId: String, serviceId: String): String?

    @Query("SELECT count(*) as numItems,sum(totalAmount) as totalAmount FROM cart_services")
    fun getCartStripData(): LiveData<CartStripData>

    @Query("SELECT serviceCartId, serviceId,parentId,name,oil_brand,strikeThrough,totalAmount,amount,cartId,title,`desc`,id,count,type FROM cart_services")
    fun getAllCartServicesData(): List<CartServices>

    @Query("DELETE FROM cart_services")
    fun deleteServices()
    @Query("DELETE FROM tyre_details ")
    fun deleteTyreList()
    @Query("DELETE FROM services_details where retail_service_type_id=:parentId")
    fun deleteServicesList(parentId: String)
    @Query("DELETE FROM services_details")
    fun deleteServicesList()
    *//*
     * Home Additional Query
     *//*
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun addHomeCategory(homeCategory: List<HomeCategory>)


    @Query("SELECT * FROM homecategory order by no")
    fun getHomeCategory(): List<HomeCategory>?
    @Query("SELECT * FROM homecategory order by no")
    fun getHomeCategoryLiveData(): LiveData<List<HomeCategory>>?
    @Query("SELECT name FROM homecategory where id=:id")
    fun getHomeCategoryName(id:String):String?
    @Query("update services_details set isAddToCart=:isAddToCart where id=:id and retail_service_type_id=:parentId")
    fun updatecartHomeServices(id:String,parentId: String,isAddToCart:Int)

    @Query("update tyre_details set isAddToCart=:isAddToCart,count=:count where id=:id")
    fun updatecartTyreServices(id:String,isAddToCart:Int,count:Int)
    @Query("update services_details set isAddToCart=:isAddToCart where retail_service_type_id=:parentId")
    fun updateCartHomeServicesByParent(parentId:Int,isAddToCart:Int)
    @Query("update services_details set isAddToCart=:isAddToCart")
    fun removeCartServiceDetail(isAddToCart: Int)

    @Query("update tyre_details set count=:isAddToCart where parent_id=:parentId")
    fun updateCartBatteryServicesByParent(parentId:Int,isAddToCart:Int)

    @Query("update services_details set id=:parentid")
    fun updateid(parentid: String)



    @Query("DELETE FROM homecategory")
    fun clearHomeCategory()

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun addHomeServices(services_details: List<ServiceDetails>)



    @Query("select * from services_details where retail_service_type_id=:cat_id")
    fun getHomeServices(cat_id:String): LiveData<List<ServiceDetails>>?





    @Query("select * from tyre_details where id=:id")
    fun getTyreModel(id:String): LiveData<TyreDetail>?

    @Query("select * from tyre_details where parent_id=:cat_id")
    fun getbatteryDetails(cat_id:String): LiveData<List<TyreDetail>>?



    @RawQuery(observedEntities = [TyreDetail::class])
    fun gettyreServices(query: SupportSQLiteQuery?): LiveData<List<TyreDetail>>
    @RawQuery(observedEntities = [TyreDetail::class])
    fun getFilterData(query: SupportSQLiteQuery?): LiveData<List<FilterProductName>>
   *//* @Query("select :filterData from tyre_details group by :filterData")
    fun getFilterData(filterData:String): LiveData<List<TyreDetail>>
*//*
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun addTyreDetails(tyre_details: List<TyreDetail>)

    @Query("DELETE FROM services_details")
    fun clearHomeServices()
    *//*
     * User Data Query
     *//*
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun addUser(user: User)

    @Query("SELECT * FROM user")
    fun getUser(): List<User>?

    @Query("DELETE FROM user")
    fun clearUser()


    *//*
     * Query chat Page API
     *//*
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun addQuery(query: List<QueryModel>)

    @Query("SELECT * FROM querModel")
    fun getQueryData(): List<QueryModel>?

    @Query("DELETE FROM querModel")
    fun clearQuery()

    @Query("SELECT remark FROM cart where id=:cartId")
    fun getRemarkData(cartId: Int) : String

    @Query("Select count(*) from cart_services")
    fun cartServicesCount():Int
    @Query("Select count(*) from cart_services where serviceId=:serviceId and parentId=:parentId")
    fun cartServicesCount(serviceId:String,parentId: String):Int*/
}
