package com.octopus_k_12.room.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.octopus_k_12.room.entities.User

@Dao
interface UserDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun addUser(user: User)

    @Query("SELECT * FROM user")
    fun getUser(): List<User>?

    @Query("DELETE FROM user")
    fun clearUser()
}