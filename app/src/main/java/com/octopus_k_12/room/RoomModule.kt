package com.octopus_k_12.room

import com.octopus_k_12.room.dao.*
import com.octopus_k_12.room.dao.DBQuery

class RoomModule(private val appDatabase: AppDatabase) {

    fun getUserDao(): UserDao {
        return appDatabase.userDao
    }




    fun getDBQuery(): DBQuery {
        return appDatabase.dbQuery
    }

}