package com.octopus_k_12.room

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.octopus_k_12.room.converters.*
import com.octopus_k_12.room.dao.*
import com.octopus_k_12.room.entities.*
import com.octopus_k_12.room.dao.DBQuery
@Database(
    entities = [User::class],
    version = 1, exportSchema = false
)
@TypeConverters(UserConverter::class)
abstract class AppDatabase : RoomDatabase() {

    abstract val userDao: UserDao
    abstract val dbQuery: DBQuery
}