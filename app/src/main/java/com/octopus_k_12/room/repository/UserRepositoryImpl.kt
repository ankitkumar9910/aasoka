package com.octopus_k_12.room.repository

import com.octopus_k_12.room.dao.UserDao
import com.octopus_k_12.room.entities.User

class UserRepositoryImpl(private val userDao: UserDao) : UserRepository {
    override fun getUser(): User? {
        userDao.getUser()?.let {
            if (it.isNotEmpty())
                return it[0]
        }
        return null
    }

    override fun addUser(user: User) {
        userDao.addUser(user)
    }

    override fun clearUser() {
        userDao.clearUser()
    }

}