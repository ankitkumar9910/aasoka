package com.octopus_k_12.room.repository

import com.octopus_k_12.room.entities.User

interface UserRepository {

    fun addUser(user: User)

    fun getUser(): User?

    fun clearUser()
}