package com.octopus_k_12.room.converters

import androidx.room.TypeConverter

import com.view.model.signup.local.AccessToken
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken

class UserConverter {
    private val gson by lazy { Gson() }
    @TypeConverter
    fun toAccessToken(str: String): AccessToken {
        val type = object : TypeToken<AccessToken>() {}.type
        return gson.fromJson(str, type)
    }

    @TypeConverter
    fun fromAccessToken(accessToken: AccessToken): String {
        val type = object : TypeToken<AccessToken>() {}.type
        return gson.toJson(accessToken, type)
    }
/*

    @TypeConverter
    fun toCar(str: String?): List<Car>? {
        val type = object : TypeToken<List<Car>>() {}.type
        return gson.fromJson(str, type)
    }

    @TypeConverter
    fun fromCar(car: List<Car>?): String {
        val type = object : TypeToken<List<Car>>() {}.type
        return gson.toJson(car, type)
    }

    @TypeConverter
    fun toUserAddressModel(str: String?): List<UserAddressModel>? {
        val type = object : TypeToken<List<UserAddressModel>>() {}.type
        return gson.fromJson(str, type)
    }

    @TypeConverter
    fun fromUserAddressModel(userAddressModel: List<UserAddressModel>?): String {
        val type = object : TypeToken<List<UserAddressModel>>() {}.type
        return gson.toJson(userAddressModel, type)
    }
*/



  /*  @TypeConverter
    fun toSubscriptionAddressModel(str: String?):SubscriptionDetailsModel?{
        val type = object : TypeToken<SubscriptionDetailsModel>() {}.type
        return gson.fromJson(str, type)
    }

    @TypeConverter
    fun fromSubscriptionAddressModel(subscriptionDetailsModel:SubscriptionDetailsModel?): String {
        val type = object : TypeToken<SubscriptionDetailsModel>() {}.type
        return gson.toJson(subscriptionDetailsModel, type)
    }

    @TypeConverter
    fun todescdetailsModel(str: String?):DescDetail?{
        val type = object : TypeToken<DescDetail>() {}.type
        return gson.fromJson(str, type)
    }

    @TypeConverter
    fun fromdescdetailsModel(descDetailsModel:DescDetail?): String {
        val type = object : TypeToken<DescDetail>() {}.type
        return gson.toJson(descDetailsModel, type)
    }

    @TypeConverter
    fun toServiceAddressModel(str: String?): List<ServiceIdModel>?{
        val type = object : TypeToken<List<ServiceIdModel>>() {}.type
        return gson.fromJson(str, type)
    }

    @TypeConverter
    fun fromServiceAddressModel(ServiceDetailsModel:List<ServiceIdModel>?): String {
        val type = object : TypeToken<List<ServiceIdModel>>() {}.type
        return gson.toJson(ServiceDetailsModel, type)
    }*/
   /* @TypeConverter
    fun toDateModel(str: String?): DeliveryDate?{
        val type = object : TypeToken<DeliveryDate>() {}.type
        return gson.fromJson(str, type)
    }

    @TypeConverter
    fun fromDateModel(deliveryDate:DeliveryDate?): String {
        val type = object : TypeToken<DeliveryDate>() {}.type
        return gson.toJson(deliveryDate, type)
    }
*/


}
