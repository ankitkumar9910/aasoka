package com.octopus_k_12.room.entities


import androidx.room.Entity
import androidx.room.PrimaryKey
import com.squareup.moshi.Json

@Entity(tableName = "user")
data class User(
    @PrimaryKey(autoGenerate = true)
    val id: Int = 1,
    @field:Json(name = "user_oid")
    val userOid: String?="",
    @field:Json(name = "activation_key")
    val activationKey: String?="",
    @field:Json(name = "Address")
    val address: String?="",
    @field:Json(name = "board")
    val board: String?="",
    @field:Json(name = "class")
    val classX: String?="",
    @field:Json(name = "created_on")
    val createdOn: String?="",
    @field:Json(name = "dob")
    val dob: String?="",
    @field:Json(name = "email_addr")
    val emailAddr: String?="",
    @field:Json(name = "first_name")
    val firstName: String?="",
    @field:Json(name = "gender")
    val gender: String?="",
    @field:Json(name = "invited_by")
    val invitedBy: String?="",
    @field:Json(name = "ip_address")
    val ipAddress: String?="",
    @field:Json(name = "is_admin")
    val isAdmin: String?="",
    @field:Json(name = "last_login")
    val lastLogin: String?="",
    @field:Json(name = "last_name")
    val lastName: String?="",
    @field:Json(name = "login_id")
    val loginId: String?="",
    @field:Json(name = "medium")
    val medium: String?="",
    @field:Json(name = "mobile_no")
    val mobileNo: String?="",
    @field:Json(name = "parent_id")
    val parentId: String?="",
    @field:Json(name = "profilepic")
    val profilepic: String?="",
    @field:Json(name = "reg_source")
    val regSource: String?="",
    @field:Json(name = "section")
    val section: String?="",
    @field:Json(name = "token")
    val token: String?="",
    @field:Json(name = "u_rolecode")
    val uRolecode: String?="",

    @field:Json(name = "user_pass")
    val userPass: String?="",
    @field:Json(name = "user_status")
    val userStatus: String?="",
    @field:Json(name = "user_type")
    val userType: String?=""
)