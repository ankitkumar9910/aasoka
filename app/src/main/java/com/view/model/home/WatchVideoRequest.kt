package com.view.model.home


import com.squareup.moshi.Json

data class WatchVideoRequest(
    @field:Json(name = "bookId")
    val bookId: String?="",
    @field:Json(name = "chapter_id")
    val chapterId: String?="",
    @field:Json(name = "grade_id")
    val gradeId: String?="",
    @field:Json(name = "length")
    val length: String?="",
    @field:Json(name = "resumeLoc")
    val resumeLoc: String?="",
    @field:Json(name = "type")
    val type: String?="",
    @field:Json(name = "userid")
    val userid: String?="",
    @field:Json(name = "videoCompletePercent")
    val videoCompletePercent: String?="",
    @field:Json(name = "watchedLength")
    val watchedLength: String?=""
)