package com.view.model.home


import com.squareup.moshi.Json

data class GetTopicRequest(
    @field:Json(name = "chapter_id")
    val chapterId: String?="",
    @field:Json(name = "subject_id")
    val subjectId: String?=""
)