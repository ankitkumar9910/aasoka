package com.view.model.home


import com.squareup.moshi.Json

data class UserConfigResponse(
    @field:Json(name = "Android_version")
    val androidVersion: String?="",
    @field:Json(name = "free_content")
    val freeContent: String?="",
    @field:Json(name = "subcriptiondetails")
    val subcriptiondetails: Subcriptiondetails?,
    @field:Json(name = "userid")
    val userid: String?="",
    @field:Json(name = "userstatus")
    val userstatus: Boolean?=false,
    @field:Json(name = "whats_new")
    val whats_new: String?="" ,
    @field:Json(name = "ncert")
    val ncert: Boolean?=false

)