package com.view.model.home


import com.squareup.moshi.Json

data class RegistrationResponse(
    @Json(name = "activation_key")
    val activationKey: String?,
    @Json(name = "email_addr")
    val emailAddr: String?,
    @Json(name = "success")
    val success: Boolean?
)