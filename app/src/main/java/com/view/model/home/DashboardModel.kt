package com.view.model.home


import com.squareup.moshi.Json

data class DashboardModel(
    @field:Json(name = "liveclass")
    val liveclass: List<Liveclas?>?=null,
    @field:Json(name = "recommend_learning")
    val recommendLearning: List<RecommendLearning?>?=null,
    @field:Json(name = "Resume_learning")
    val resumeLearning: List<ResumeLearning?>?=null,
    @field:Json(name = "subject")
    val subject: List<Subject?>?=null
)