package com.view.model.home


import com.squareup.moshi.Json

data class RegistrationModelRequest(
    @field:Json(name = "address")
    val address: String?,
    @field:Json(name = "board")
    val board: String?,
    @field:Json(name = "class")
    val classX: String?,
    @field:Json(name = "date_of_birth")
    val dateOfBirth: String?,
    @field:Json(name = "emailaddress")
    val emailaddress: String?,
    @field:Json(name = "first_name")
    val firstName: String?,
    @field:Json(name = "gender")
    val gender: String?,
    @field:Json(name = "last_name")
    val lastName: String?,
    @field:Json(name = "medium")
    val medium: String?,
    @field:Json(name = "phonenumber")
    val phonenumber: String?,
    @field:Json(name = "user_type")
    val userType: String?
)