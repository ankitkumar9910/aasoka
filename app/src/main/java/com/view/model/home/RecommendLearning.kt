package com.view.model.home


import com.squareup.moshi.Json

data class RecommendLearning(
    @field:Json(name = "chapter_id")
    val chapterId: String?="",
    @field:Json(name = "chapter_name")
    val chapterName: String?="",
    @field:Json(name = "grade_id")
    val gradeId: String?="",
    @field:Json(name = "grade_name")
    val gradeName: String?="",
    @field:Json(name = "is_pass")
    val isPass: String?="",
    @field:Json(name = "percentage")
    val percentage: String?="",
    @field:Json(name = "subject_id")
    val subjectId: String?="",
    @field:Json(name = "subject_name")
    val subjectName: String?=""
)