package com.view.model.home


import com.squareup.moshi.Json

data class Liveclas(
    @field:Json(name = "board")
    val board: String?="",
    @field:Json(name = "classlink")
    val classlink: String?="",
    @field:Json(name = "classname")
    val classname: String?="",
    @field:Json(name = "created_on")
    val createdOn: String?="",
    @field:Json(name = "endtime")
    val endtime: String?="",
    @field:Json(name = "grade")
    val grade: String?="",
    @field:Json(name = "lclass_id")
    val lclassId: String?="",
    @field:Json(name = "meetingId")
    val meetingId: String?="",
    @field:Json(name = "meetingcode")
    val meetingcode: String?="",
    @field:Json(name = "startDate")
    val startDate: String?="",
    @field:Json(name = "subject")
    val subject: String?="",
    @field:Json(name = "userid")
    val userid: String?=""
)