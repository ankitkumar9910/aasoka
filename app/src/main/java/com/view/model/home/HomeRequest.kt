package com.view.model.home


import com.squareup.moshi.Json

data class HomeRequest(
    @field:Json(name = "userid")
    val userid: Int?
)