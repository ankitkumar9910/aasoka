package com.view.model.home


import com.squareup.moshi.Json

data class Subcriptiondetails(
    @Json(name = "day_left")
    val dayLeft: Int?,
    @Json(name = "Plan name")
    val planName: String?,
    @Json(name = "subscription_date")
    val subscriptionDate: String?
)