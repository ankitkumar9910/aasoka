package com.view.model.home


import com.squareup.moshi.Json

data class GetTopicResponse(
    @field:Json(name = "topic_id")
    val topicId: String?="",
    @field:Json(name = "topic_name")
    val topicName: String?="",
    @field:Json(name = "video_url")
    val videoUrl: String?=""
)