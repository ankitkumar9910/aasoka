package com.view.model.home


import com.squareup.moshi.Json

data class Subject(
    @field:Json(name = "enrollment_id")
    val enrollmentId: String?="",
    @field:Json(name = "subject_id")
    val subjectId: String?="",
    @field:Json(name = "subject_name")
    val subjectName: String?="",
    @field:Json(name = "image_url")
    val imageUrl: String?="",
    var isSelection:Boolean=false
)