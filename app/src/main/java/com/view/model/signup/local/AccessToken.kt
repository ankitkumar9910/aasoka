package com.view.model.signup.local

import com.squareup.moshi.Json

data class AccessToken(
    @field:Json(name= "token") val token: String
)