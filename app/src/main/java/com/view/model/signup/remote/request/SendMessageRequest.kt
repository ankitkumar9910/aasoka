package com.view.model.signup.remote.request

import com.squareup.moshi.Json

data class SendMessageRequest(
    @field:Json(name = "phonenumber") val phoneNumber: String,
    @field:Json(name = "otp") val otp: String
)