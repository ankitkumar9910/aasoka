package com.view.model.signup.remote.request

import com.squareup.moshi.Json

data class VerifyOtpRequest(
    @field:Json(name= "phonenumber") val phoneNumber: String,
    @field:Json(name= "otp") val otp: String,
    @field:Json(name= "oldUserId") val oldUserId: String?=""



)