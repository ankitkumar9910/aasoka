package com.view.model.signup.local

sealed class ResultState {
    data class Success<out T>(val data: T?,val position : Int) : ResultState()
    data class Failure(val errorMessage: String, val errorCode: Int) : ResultState()

}