package com.view.model.signup.remote.response

import com.squareup.moshi.Json

data class GetConfigResponse(
    @field:Json(name= "id") var id: Int?,
    @field:Json(name= "key") var key: String? = "",
    @field:Json(name= "value") var value: String? = "",
    @field:Json(name= "value_type") var value_type: String? = "",
    @field:Json(name= "custom_token") var customToken: String? = "",
    @field:Json(name= "access_token") var accessToken: String? = "",
    @field:Json(name= "is_hard") var is_hard: Int?
)