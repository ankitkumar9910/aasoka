package com.view.model.signup.local

import com.octopus_k_12.room.entities.User

sealed class VerifyReferralState {
    data class Success(val message: User) : VerifyReferralState()
    data class Failure(val errorMessage: String, val errorCode: Int) : VerifyReferralState()
}