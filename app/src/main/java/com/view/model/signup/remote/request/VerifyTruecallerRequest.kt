package com.view.model.signup.remote.request


import com.squareup.moshi.Json

data class VerifyTruecallerRequest(
    @field:Json(name= "avatar_url")
    val avatarUrl: String?="",
    @field:Json(name= "city")
    val city: String?="",
    @field:Json(name= "company_name")
    val companyName: String?="",
    @field:Json(name= "country_code")
    val countryCode: String?="",
    @field:Json(name= "email")
    val email: String?="",
    @field:Json(name= "facebook_id")
    val facebookId: String?="",
    @field:Json(name= "first_name")
    val firstName: String?="",
    @field:Json(name= "gender")
    val gender: String?="",
    @field:Json(name= "is_ambassador")
    val isAmbassador: String?="",
    @field:Json(name= "is_sim_changed")
    val isSimChanged: String?="",
    @field:Json(name= "is_trueName")
    val isTrueName: String?="",
    @field:Json(name= "job_title")
    val jobTitle: String?="",
    @field:Json(name= "last_name")
    val lastName: String?="",
    @field:Json(name= "number")
    val number: String?="",
    @field:Json(name= "payload")
    val payload: String?="",
    @field:Json(name= "request_nonce")
    val requestNonce: String?="",
    @field:Json(name= "signature")
    val signature: String?="",
    @field:Json(name= "signature_algorithm")
    val signatureAlgorithm: String?="",
    @field:Json(name= "street")
    val street: String?="",
    @field:Json(name= "twitter_id")
    val twitterId: String?="",
    @field:Json(name= "url")
    val url: String?="",
    @field:Json(name= "user_locale")
    val userLocale: String?="",
    @field:Json(name= "verification_mode")
    val verificationMode: String?="",
    @field:Json(name= "verification_timestamp")
    val verificationTimestamp: String?="",
    @field:Json(name= "login_type")
    val login_type: String?="",
    @field:Json(name= "zipcode")
    val zipcode: String?=""
)