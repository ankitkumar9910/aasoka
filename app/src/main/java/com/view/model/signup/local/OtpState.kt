package com.view.model.signup.local


sealed class OtpState {
    data class Success(val message: String) : OtpState()
    data class Failure(val errorMessage: String, val errorCode: Int) : OtpState()
}