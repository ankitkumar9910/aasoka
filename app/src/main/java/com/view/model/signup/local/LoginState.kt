package com.view.model.signup.local

import com.octopus_k_12.room.entities.User

sealed class LoginState {
    data class Success(val data: User) : LoginState()
    data class Failure(val errorMessage: String, val errorCode: Int) : LoginState()
}