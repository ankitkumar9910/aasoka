package com.view.model.signup.remote.response

import com.squareup.moshi.Json

data class SendMessageResponse(
    @field:Json(name= "otp") val otp: String,
    @field:Json(name= "phonenumber") val phonenumber: String
)