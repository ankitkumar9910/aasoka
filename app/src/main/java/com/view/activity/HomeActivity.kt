package com.view.activity

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.content.IntentSender.SendIntentException
import android.content.pm.PackageManager
import android.location.LocationManager
import android.net.ConnectivityManager
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.crashlytics.android.Crashlytics
import com.google.android.material.snackbar.Snackbar
import com.google.android.play.core.appupdate.AppUpdateManager
import com.google.android.play.core.appupdate.AppUpdateManagerFactory
import com.google.android.play.core.install.InstallState
import com.google.android.play.core.install.InstallStateUpdatedListener
import com.google.android.play.core.install.model.AppUpdateType
import com.google.android.play.core.install.model.InstallStatus
import com.google.android.play.core.install.model.UpdateAvailability
import com.network.core.BaseActivity
import com.network.core.BaseFragment
import com.network.core.BaseViewModel
import com.network.utils.Constants
import com.octopus_k_12.App
import com.octopus_k_12.R
import com.octopus_k_12.utils.*
import com.view.viewmodel.SignUpViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel
import timber.log.Timber

class HomeActivity : BaseActivity<BaseViewModel>(), FragmentNavigation, ConnectivityReceiver.ConnectivityReceiverListener {
    private val appUpdateManager: AppUpdateManager by lazy { AppUpdateManagerFactory.create(this) }
    private var exitAppHandler: Handler? = null
    override val viewModel: SignUpViewModel by viewModel()

    private var connectivityManager = ConnectivityReceiver()


    companion object {
        private const val APP_UPDATE_REQUEST_CODE = 1991
    }

    override fun getLayoutRes(): Int {
        return R.layout.activity_main
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        exitAppHandler = Handler(Looper.getMainLooper())

        init()
    }

    private val appUpdatedListener: InstallStateUpdatedListener by lazy {
        object : InstallStateUpdatedListener {
            override fun onStateUpdate(installState: InstallState) {
                when {
                    installState.installStatus() == InstallStatus.DOWNLOADED -> popupSnackbarForCompleteUpdate()
                    installState.installStatus() == InstallStatus.INSTALLED -> appUpdateManager.unregisterListener(this)
                    else -> Timber.d("InstallStateUpdatedListener: state: %s", installState.installStatus())
                }
            }
        }
    }

    override fun onStart() {
        super.onStart()

    }
    public override fun onNewIntent(intent: Intent) {
        super.onNewIntent(intent)
        intent.getStringExtra(Constants.CHAT_NOTIFICATION)?.let {
            if (it.isNotEmpty()) {
                viewModel.setSharedPreferences(Constants.CHAT_NOTIFICATION, it)
            }
        }
        try {
            intent.putExtra("branch_force_new_session", true)
        } catch (e: Exception) {
        }
        setIntent(intent)

    }

    override fun onNetworkConnectionChanged(isConnected: Boolean) {
      //  EventBus.getDefault().post(ConnectivityEvent(isConnected.toString()))
    }

    private fun checkPermissions(): Boolean {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            return true
        }
        return false
    }

    private fun isLocationEnabled(): Boolean {
        val locationManager: LocationManager = getSystemService(Context.LOCATION_SERVICE) as LocationManager
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) || locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)
    }

    @Suppress("DEPRECATION")
    private fun init() {

        registerReceiver(connectivityManager, IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION))
        ConnectivityReceiver.connectivityReceiverListener = this
        checkForAppUpdate()
        val bundle = Bundle()
        val fragment: BaseFragment<*>?
       if (viewModel.getSharedPreferencesString(Constants.ACCESS_TOKEN, "").isEmpty()) {
            changeStatusBarColor(R.color.colorLightGray)
            fragment = FragmentFactory.fragment(FragmentFactory.Screens.ONBOARDING_FRAGMENT, null)
        } else if (viewModel.getSharedPreferencesString(Constants.USER_NAME, "").isEmpty() || viewModel.getSharedPreferencesString(Constants.USER_EMAIL, "").isEmpty()) {
            bundle.putString(Constants.OPEN_SCREEN_TYPE, Constants.OPEN_SCREEN_TYPE_USER_EDIT)
            bundle.putBoolean(Constants.IS_FROM_START, true)
            fragment = FragmentFactory.fragment(FragmentFactory.Screens.USER_REGISTRATION_FRAGMENT, bundle)
        }  else {
            fragment = FragmentFactory.fragment(FragmentFactory.Screens.BOTTOM_FRAGMENT, bundle)

        }
        fragment?.let { baseFrag ->
            replaceToBackStack(this, R.id.fragmentLayout, baseFrag)
        }
    }

    private fun checkForAppUpdate() {
        try {
            // Returns an intent object that you use to check for an update.
            val appUpdateInfoTask = appUpdateManager.appUpdateInfo
            // Checks that the platform will allow the specified type of update.
            appUpdateInfoTask.addOnSuccessListener { appUpdateInfo ->
                try {
                    if (appUpdateInfo.updateAvailability() == UpdateAvailability.UPDATE_AVAILABLE) {
                        // Request the update.
                        val installType = when {
                            appUpdateInfo.isUpdateTypeAllowed(AppUpdateType.FLEXIBLE) -> AppUpdateType.FLEXIBLE
                            appUpdateInfo.isUpdateTypeAllowed(AppUpdateType.IMMEDIATE) -> AppUpdateType.IMMEDIATE
                            else -> null
                        }
                        if (installType == AppUpdateType.FLEXIBLE) appUpdateManager.registerListener(appUpdatedListener)

                        appUpdateManager.startUpdateFlowForResult(appUpdateInfo, installType!!, this, APP_UPDATE_REQUEST_CODE)
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        } catch (e: Exception) {
            Crashlytics.logException(e)
        }
    }

    private fun popupSnackbarForCompleteUpdate() {
        val snackbar = Snackbar.make(findViewById(R.id.const_main), getString(R.string.app_update), Snackbar.LENGTH_INDEFINITE)
        snackbar.setAction(getString(R.string.restart)) { appUpdateManager.completeUpdate() }
        snackbar.setActionTextColor(ContextCompat.getColor(this, R.color.design_default_color_on_primary))
        snackbar.show()
    }

    public override fun onResume() {
        super.onResume()
        // defferdDeepLink()
        appUpdateManager.appUpdateInfo.addOnSuccessListener { appUpdateInfo ->
            // If the update is downloaded but not installed,
            // notify the user to complete the update.
            //Check if Immediate update is required
            try {
                if (appUpdateInfo.installStatus() == InstallStatus.DOWNLOADED) {
                    popupSnackbarForCompleteUpdate()
                }
                if (appUpdateInfo.updateAvailability() == UpdateAvailability.DEVELOPER_TRIGGERED_UPDATE_IN_PROGRESS) {
                    // If an in-app update is already running, resume the update.
                    appUpdateManager.startUpdateFlowForResult(appUpdateInfo, AppUpdateType.IMMEDIATE, this, APP_UPDATE_REQUEST_CODE)
                }
            } catch (e: SendIntentException) {
                e.printStackTrace()
            }
        }
    }





    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == APP_UPDATE_REQUEST_CODE) {
            if (resultCode != Activity.RESULT_OK) {
                Toast.makeText(this, getString(R.string.app_update_failed), Toast.LENGTH_SHORT).show()
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        unregisterReceiver(connectivityManager)
    }
}

