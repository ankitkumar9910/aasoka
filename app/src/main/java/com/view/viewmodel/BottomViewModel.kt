package com.view.viewmodel

import android.app.Application
import com.network.core.BaseScopeViewModel
import com.network.retrofit.retroApi.ApiRepository

class BottomViewModel(app: Application, private val apiRepo: ApiRepository) : BaseScopeViewModel(app) {

    var homeAPIUpdateVersion : Long = 0

    fun get() : Long  {
        return homeAPIUpdateVersion
    }

    fun set(value: Long) {
        homeAPIUpdateVersion = value
    }


}