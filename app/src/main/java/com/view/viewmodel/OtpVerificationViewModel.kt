package com.view.viewmodel

import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.network.common.Event
import com.network.common.Result
import com.network.core.BaseScopeViewModel
import com.network.extension.awaitAndGet
import com.octopus_k_12.room.repository.UserRepository
import com.view.model.signup.local.LoginState
import com.network.retrofit.retroApi.OtpRepository
import kotlinx.coroutines.launch

public class OtpVerificationViewModel(app: Application, private val otpRepository: OtpRepository, private val userRepository: UserRepository) : BaseScopeViewModel(app) {
    fun verifyOtp(phoneNumber: String, otp: String, oldUserId: String) {
        launch {
            val result = otpRepository.verifyOtpAsync(phoneNumber, otp,oldUserId).awaitAndGet()
            otpVerificationStatusLiveData.postValue(Event(when (result) {
                is Result.Failure -> LoginState.Failure(result.errorMessage, result.code)
                is Result.Success -> {
                    result.body.data?.let { data ->
                       // userRepository.addUser(data[0]
                        LoginState.Success(data.get(0))
                    }
                }
            }))

        }

    }

    private lateinit var otpVerificationStatusLiveData: MutableLiveData<Event<LoginState>>
    fun getOtpVerificationStatus(): LiveData<Event<LoginState>> {
        if (!::otpVerificationStatusLiveData.isInitialized) {
            otpVerificationStatusLiveData = MutableLiveData()
        }
        return otpVerificationStatusLiveData
    }

}