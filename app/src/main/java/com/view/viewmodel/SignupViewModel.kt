package com.view.viewmodel

import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.network.common.Event
import com.network.common.Result
import com.network.core.BaseScopeViewModel
import com.network.extension.awaitAndGet
import com.view.model.signup.local.OtpState
import com.network.retrofit.retroApi.LoginRepository
import kotlinx.coroutines.launch

class   SignUpViewModel(app: Application, private val loginRepository: LoginRepository) : BaseScopeViewModel(app) {

    fun sendMessage(phoneNumber: String,otp:String,hash: String) {
        launch {
            val result = loginRepository.sendMessageAsync(phoneNumber,otp).awaitAndGet()
            signUpStatusLiveData.postValue(Event(when (result) {
                is Result.Failure -> OtpState.Failure(result.errorMessage, result.code)
                is Result.Success -> result.body.run {
                    // Timber.tag("TEST").d(message.toString())
                    OtpState.Success(result.body.message)
                }
            }))
        }

    }



    private lateinit var signUpStatusLiveData: MutableLiveData<Event<OtpState>>
    fun getSignupStatus(): LiveData<Event<OtpState>> {
        if (!::signUpStatusLiveData.isInitialized)
            signUpStatusLiveData = MutableLiveData()
        return signUpStatusLiveData
    }

    fun getSharedPreferencesString(key: String, default: String): String {
        return loginRepository.getSharedPreferencesString(key, default)
    }

    fun setSharedPreferences(key: String, value: String) {
        loginRepository.setSharedPreferences(key, value)
    }

}