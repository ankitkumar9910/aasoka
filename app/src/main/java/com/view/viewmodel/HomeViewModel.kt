package com.view.viewmodel

import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import kotlinx.coroutines.launch
import com.network.common.Event
import com.network.common.Result
import com.network.common.ResultState
import com.network.core.BaseScopeViewModel
import com.network.extension.awaitAndGet
import com.network.retrofit.retroApi.ApiRepository
import com.view.model.home.GetTopicRequest
import com.view.model.home.HomeRequest
import com.view.model.home.RegistrationModelRequest
import com.view.model.home.WatchVideoRequest

class HomeViewModel (app: Application, private val apiRepository: ApiRepository) : BaseScopeViewModel(app) {
      fun dashbordApi(userId:Int) {
    launch {
           val result = apiRepository.dashboardApi(userId).awaitAndGet()
        dashboardLiveData.postValue(Event(when (result) {
            is Result.Failure -> ResultState.Failure(result.errorMessage, result.code)
            is Result.Success -> {
                ResultState.Success(result.body.data) }
           })) } }
    private lateinit var dashboardLiveData: MutableLiveData<Event<ResultState>>
   fun getDashboardLiveData(): LiveData<Event<ResultState>> {
       if (!::dashboardLiveData.isInitialized) {
           dashboardLiveData = MutableLiveData()
       }
       return dashboardLiveData
   }
    private lateinit var subjectLiveData: MutableLiveData<Event<ResultState>>
    fun getapichaptersandtopicsLiveData(): LiveData<Event<ResultState>> {
        if (!::subjectLiveData.isInitialized) {
            subjectLiveData = MutableLiveData()
        }
        return subjectLiveData
    }

    private lateinit var topicLiveData: MutableLiveData<Event<ResultState>>
     fun getTopicLiveData(): LiveData<Event<ResultState>> {
         if (!::topicLiveData.isInitialized) {
             topicLiveData = MutableLiveData()
         }
         return topicLiveData
     }
     fun getTopicApi(request: GetTopicRequest) {
         launch {
             val result = apiRepository.getapisubtopic(request).awaitAndGet()
             topicLiveData.postValue(Event(when (result) {
                 is Result.Failure -> ResultState.Failure(result.errorMessage, result.code)
                 is Result.Success -> {

                     ResultState.Success(result.body.data)
                 }
             })) } }

     private lateinit var subscriptionLiveData: MutableLiveData<Event<ResultState>>


     private lateinit var userConfigLiveData: MutableLiveData<Event<ResultState>>
     fun getUserConfigLiveData(): LiveData<Event<ResultState>> {
         if (!::userConfigLiveData.isInitialized) {
             userConfigLiveData = MutableLiveData()
         }
         return userConfigLiveData
     }
     fun  getUserConfigApi(userId: Int) {
         launch {
             val result = apiRepository.getUserConfigApi(HomeRequest(userId)).awaitAndGet()
             userConfigLiveData.postValue(Event(when (result) {
                 is Result.Failure -> ResultState.Failure(result.errorMessage, result.code)
                 is Result.Success -> {

                     ResultState.Success(result.body.data)
                 }
             }))

         }}

     private lateinit var precticeTestLiveData: MutableLiveData<Event<ResultState>>
     fun getPrecticeTestLiveData(): LiveData<Event<ResultState>> {
         if (!::precticeTestLiveData.isInitialized) {
             precticeTestLiveData = MutableLiveData()
         }
         return precticeTestLiveData
     }




     fun  trackcontentapi(request: WatchVideoRequest) {
         launch {
          apiRepository.trackcontentapi(request)

         }}
     fun clearAllSharedPrefrences() {
         apiRepository.clearAllSharedPrefrences()
     }

     fun getSharedPreferencesString(key: String, default: String): String {
        return apiRepository.getSharedPreferencesString(key, default)
    }

    fun setSharedPreferences(key: String, value: String) {
        apiRepository.setSharedPreferences(key, value)
    }
}