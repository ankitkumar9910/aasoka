package com.view.fragment

import android.os.Bundle
import android.view.View
import com.network.core.BaseViewModel
import com.octopus_k_12.R
import com.octopus_k_12.base.BaseWrapperFragment
import com.view.viewmodel.OtpVerificationViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel

class ProgresFragment  : BaseWrapperFragment<BaseViewModel>(){

    override val viewModel: OtpVerificationViewModel by viewModel()

    override fun getLayoutRes(): Int {
        return R.layout.fragment_progress
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        init()

    }

    private fun init() {



    }}
