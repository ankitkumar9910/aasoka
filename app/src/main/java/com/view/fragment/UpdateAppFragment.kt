package com.view.fragment

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.View
import com.network.core.BaseDialogFragment
import com.network.core.BaseViewModel
import com.network.utils.Constants
import com.octopus_k_12.R
import com.view.viewmodel.HomeViewModel
import kotlinx.android.synthetic.main.fragment_update.*
import org.koin.androidx.viewmodel.ext.android.viewModel
import kotlin.reflect.jvm.internal.impl.load.java.Constant

class UpdateAppFragment : BaseDialogFragment<BaseViewModel>()
     {

    override val viewModel: HomeViewModel by viewModel()

    override fun getLayoutRes(): Int {
        return R.layout.fragment_update
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(STYLE_NORMAL, R.style.FullScreenDialogStyle)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        this.isCancelable=false
        init()
    }

    private fun init() {
        arguments?.let {
            arg->
            tvWhatsNewDescription.text=arg.getString(Constants.UPDATE_DESCRIPTION,"")
        }
        tv_Update.setOnClickListener(){

            startActivity(
                Intent(
                    Intent.ACTION_VIEW,
                    Uri.parse("https://play.google.com/store/apps/details?id=com.octopus_k_12")
                )
            )
        }

    }




}