package com.view.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.widget.TextView
import androidx.activity.addCallback
import androidx.core.content.ContextCompat
import androidx.viewpager.widget.ViewPager
import com.network.core.BaseViewModel
import com.octopus_k_12.R
import com.octopus_k_12.base.BaseWrapperFragment
import com.octopus_k_12.utils.FragmentFactory
import com.octopus_k_12.utils.FragmentNavigation
import com.octopus_k_12.utils.HtmlCompat
import com.view.viewmodel.HomeViewModel
import kotlinx.android.synthetic.main.bottom_fragment.*
import kotlinx.android.synthetic.main.fragment_onboarding.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class OnBoardingFragment : BaseWrapperFragment<BaseViewModel>(), FragmentNavigation {
    private var dots: Array<TextView?> = emptyArray()
    private var viewLayout: View? = null
    override fun getLayoutRes(): Int {
        return R.layout.fragment_onboarding
    }

    override val viewModel: HomeViewModel by viewModel()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        viewLayout = inflater.inflate(getLayoutRes(), container, false)
        return viewLayout
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        init()
    }

    private fun init() {
        requireActivity().onBackPressedDispatcher.addCallback(this) {
            requireActivity().finish()
        }
        // changeStatusBarColor(R.color.colorDarkOrange)
        tv_getStarted.setOnClickListener(){
            if (viewFlipper.displayedChild != 2) {
                viewFlipper.inAnimation = AnimationUtils.loadAnimation(activity, R.anim.in_from_right)
                viewFlipper.outAnimation = AnimationUtils.loadAnimation(activity, R.anim.out_from_left)
                viewFlipper.showNext()
                if(viewFlipper.displayedChild==2){
                    tv_getStarted.text=getString(R.string.continue_text)
                }else{
                    tv_getStarted.text=getString(R.string.next)
                }
                addBottomDots(viewFlipper.displayedChild)
            }else{

            addFrament(FragmentFactory.Screens.SIGNUP_FRAGMENT,null)}
        }
        addBottomDots(0)
        var initialX = 0f
        viewLayout?.setOnTouchListener { _, event ->
            when (event.action) {
                MotionEvent.ACTION_DOWN -> {
                    initialX = event.x
                }
                MotionEvent.ACTION_UP -> {
                    val finalX = event.x

                    if (initialX > finalX) {
                        if (viewFlipper.displayedChild != 2) {
                            viewFlipper.inAnimation = AnimationUtils.loadAnimation(activity, R.anim.in_from_right)
                            viewFlipper.outAnimation = AnimationUtils.loadAnimation(activity, R.anim.out_from_left)
                            viewFlipper.showNext()
                        }
                    } else {
                        if (viewFlipper.displayedChild != 0) {
                            viewFlipper.inAnimation = AnimationUtils.loadAnimation(activity, R.anim.in_from_left)
                            viewFlipper.outAnimation = AnimationUtils.loadAnimation(activity, R.anim.out_from_right)
                            viewFlipper.showPrevious()
                        }
                    }
                    addBottomDots(viewFlipper.displayedChild)
                    if(viewFlipper.displayedChild==2){
                        tv_getStarted.text=getString(R.string.continue_text)
                    }else{
                        tv_getStarted.text=getString(R.string.next)
                    }
                }

            }
            true
        }

    }


    private fun addBottomDots(currentPage: Int) {
        dots = arrayOfNulls(3)
        val colorsActive = R.color.white
        llDotsView.removeAllViews()
        for (i in dots.indices) {
            val textView = TextView(activity)
            textView.text = HtmlCompat.fromHtml("&#9675;")
            textView.textSize = 25f
            textView.setPadding(0, 0, 20, 0)
            textView.setTextColor(ContextCompat.getColor(requireActivity(), colorsActive))
            dots[i] = textView
            llDotsView.addView(dots[i])
        }

        if (dots.isNotEmpty()) dots[currentPage]?.text = HtmlCompat.fromHtml("&#9679;")
    }
}
