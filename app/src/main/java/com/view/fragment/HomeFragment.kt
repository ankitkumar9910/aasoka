package com.view.fragment

import android.os.Bundle
import android.view.View
import com.network.core.BaseViewModel
import com.octopus_k_12.R
import com.octopus_k_12.base.BaseWrapperFragment
import com.view.viewmodel.HomeViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel


class HomeFragment : BaseWrapperFragment<BaseViewModel>() {

    override val viewModel: HomeViewModel by viewModel()

    override fun getLayoutRes(): Int {
        return R.layout.fragment_home
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


    }



}


// click handling category items , rsa view , popular search and insurance

