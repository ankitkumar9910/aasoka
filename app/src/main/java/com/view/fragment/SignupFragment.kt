package com.view.fragment

import android.annotation.SuppressLint
import android.app.Activity

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.network.core.BaseActivity
import com.network.core.BaseViewModel
import com.network.extension.showLongToast
import com.network.extension.showShortToast
import com.network.helper.KeyboardUtil
import com.network.utils.Constants
import com.google.android.gms.auth.api.phone.SmsRetriever
import com.octopus_k_12.R
import com.view.model.signup.local.OtpState
import com.view.viewmodel.SignUpViewModel
import android.view.inputmethod.EditorInfo
import androidx.activity.addCallback
import com.octopus_k_12.utils.FragmentNavigation
import com.truecaller.android.sdk.*
import com.network.common.EventObserver
import com.octopus_k_12.utils.*
import com.octopus_k_12.base.BaseWrapperFragment

import kotlinx.android.synthetic.main.fragment_signup_new.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class SignUpFragment : BaseWrapperFragment<BaseViewModel>(), FragmentNavigation, ITrueCallback {
    private var viewLayout: View? = null
    private var referralCode: String? = ""

    override fun getLayoutRes(): Int {
        return R.layout.fragment_signup_new
    }


    private val signupViewModel: SignUpViewModel by viewModel()
    override val viewModel: SignUpViewModel by viewModel()
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        viewLayout = inflater.inflate(getLayoutRes(), container, false)
        return viewLayout
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        init()
    }

    @SuppressLint("ClickableViewAccessibility")
    private fun init() {
        requireActivity().onBackPressedDispatcher.addCallback(this) {
            requireActivity().finish()
        }


        viewModel.getSharedPreferencesString(Constants.COUNTRY_CODE,"+91").let {
            tv_country_code.text=it
        }
        //addBottomDots(0)
        setObserver()

        referralCode = arguments?.getString(Constants.REFERRAL_CODE) ?: ""

        mobile_number.setOnEditorActionListener { _, actionId, _ ->
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                validateMobileNumber()
                true
            } else {
                false
            }
        }
        btSubmit.setOnClickListener {
            Bundle().apply {
                FirebaseAnalyticsLog.trackFireBaseEventLog(
                    FirebaseEventNameENUM.FBE_INIT_LOGIN,
                    this
                )
            }
            validateMobileNumber()
        }
        val client = SmsRetriever.getClient(activity as BaseActivity<*>)
        val task = client.startSmsRetriever()
        task.addOnSuccessListener {}
        task.addOnFailureListener {}
       // setTrueCaller()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (TrueSDK.getInstance().onActivityResultObtained(activity as Activity, resultCode, data)) {

            return
        }

    }

    private fun validateMobileNumber() {
        val mobileNumber = mobile_number.text.toString()
        val pattern = "^[0-9]{10}\$".toRegex() //"^[+]?[0-9]{10,13}\$".toRegex()
        if (pattern.matches(mobileNumber)) {
            btSubmit.visibility = View.INVISIBLE
            progress_circular.visibility = View.VISIBLE
            val helper: AppSignatureHelper = context?.let { it1 -> AppSignatureHelper(it1) }!!
            viewModel.sendMessage(mobileNumber,"", helper.getAppSignatures()[0])
        } else {
            activity?.showLongToast(getString(R.string.phone_number_message))
        }
    }

    private fun setObserver() {
        viewModel.getSignupStatus().observe(viewLifecycleOwner, EventObserver {
            when (it) {
                is OtpState.Success -> {
                    btSubmit.visibility = View.VISIBLE
                    progress_circular.visibility = View.GONE
                    val mobileNumber = mobile_number.text.toString()
                    KeyboardUtil.hideKeyboard(activity)
                    val bundle = Bundle()
                    bundle.putString(Constants.PHONE_NUMBER, mobileNumber)
                    bundle.putString(Constants.REFERRAL_CODE, referralCode)
                    addFrament(FragmentFactory.Screens.OTP_FRAGMENT, bundle)
                }
                is OtpState.Failure -> {
                    progress_circular.visibility = View.GONE
                    btSubmit.visibility = View.VISIBLE
                    activity?.showShortToast(it.errorMessage)
                }
            }
        })


    }

    private fun setTrueCaller() {
        val trueScope: TrueSdkScope = TrueSdkScope.Builder(requireActivity(), this).consentMode(TrueSdkScope.CONSENT_MODE_POPUP).consentTitleOption(TrueSdkScope.SDK_CONSENT_TITLE_VERIFY).footerType(TrueSdkScope.FOOTER_TYPE_SKIP).build()
        TrueSDK.init(trueScope)
        if (TrueSDK.getInstance().isUsable) {
            TrueSDK.getInstance().getUserProfile(this)
        }
    }


    override fun onFailureProfileShared(p0: TrueError) {
    }

    override fun onSuccessProfileShared(profile: TrueProfile) {
        var number: String = profile.phoneNumber ?: ""
        if (number.length > 10) {
            number = number.substring(number.length - 10, number.length)
        }
        btSubmit?.visibility = View.GONE
        showRoundProgressBar(true)
          }

    override fun onVerificationRequired() {
    }
}