
package com.view.fragment

import android.os.Bundle
import android.view.View
import androidx.activity.addCallback
import androidx.fragment.app.Fragment
import com.network.core.BaseFragment
import com.network.utils.Constants
import com.octopus_k_12.R
import com.octopus_k_12.base.BaseBottomFragment

import kotlinx.android.synthetic.main.bottom_fragment.*
import com.view.viewmodel.BottomViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel
import java.util.*


class BottomFragment : BaseBottomFragment<BottomViewModel>() {
    override fun getLayoutRes(): Int {
        return R.layout.bottom_fragment
    }

    private lateinit var startTime: Date
    override val viewModel: BottomViewModel by viewModel()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        init()
    }

    private fun init() {
        requireActivity().onBackPressedDispatcher.addCallback(this) {
            if (getViewPager()?.currentItem != Constants.TABS.HOME_TABS) {
                setBottomTab(Constants.TABS.HOME_TABS)
            } else {
                requireActivity().finish()
            }
        }
        bottomNavigationView.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)
        setupViewPager(getViewPager())


       // setDefferedDeepLink()
    }








    /*
     * Redirecetion to bottom fragment or new fragment
     */

    /*
     * Remove all stack fragment till bottom fragment
     */
    private fun removeTopFragments() {
        fragmentManager?.let { fm ->
            val fragList: List<Fragment>? = fm.fragments
            fragList?.let { frags ->
                for (fragment in frags) {
                    if (fragment !is BottomFragment && fragment is BaseFragment<*>) {
                        popBackStack()
                    }
                }
            }
        }
    }

    /*
     * Send firebase token and instance id to server
     */



}