package com.view.fragment

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.View
import com.network.core.BaseViewModel
import com.network.utils.Constants
import com.octopus_k_12.App
import com.octopus_k_12.R
import com.octopus_k_12.base.BaseWrapperFragment
import com.octopus_k_12.utils.FragmentFactory
import com.octopus_k_12.utils.Utils
import com.view.activity.HomeActivity
import com.view.viewmodel.HomeViewModel
import kotlinx.android.synthetic.main.fragment_more.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class MoreFragment  : BaseWrapperFragment<BaseViewModel>(){

    override val viewModel: HomeViewModel by viewModel()

    override fun getLayoutRes(): Int {
        return R.layout.fragment_more
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        init()


    }
    private fun init(){
        tv_Logout.setOnClickListener(){
            viewModel.clearAllSharedPrefrences()
            (activity?.application as App).restartKoin()
            (context as HomeActivity).finish()
            startActivity(Intent(activity, HomeActivity::class.java))
        }
    }

}
