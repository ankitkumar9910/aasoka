package com.view.fragment

import android.content.IntentFilter
import android.os.Bundle
import android.os.CountDownTimer
import android.view.MenuItem
import android.view.View
import androidx.activity.addCallback
import com.network.common.EventObserver
import com.network.core.BaseActivity
import com.network.core.BaseViewModel
import com.network.extension.makeLinks
import com.network.extension.showLongToast
import com.network.extension.showShortToast
import com.network.helper.KeyboardUtil
import com.network.helper.OtpFilledCallback
import com.network.utils.Constants

import com.octopus_k_12.R
import com.octopus_k_12.utils.*
import com.octopus_k_12.base.BaseWrapperFragment
import com.view.model.signup.local.LoginState
import com.view.viewmodel.OtpVerificationViewModel
import com.view.viewmodel.SignUpViewModel
import kotlinx.android.synthetic.main.fragment_opt_verification.*
import kotlinx.android.synthetic.main.fragment_opt_verification.btSubmit
import com.view.model.signup.local.OtpState
import org.koin.androidx.viewmodel.ext.android.viewModel

class OtpFragment : BaseWrapperFragment<BaseViewModel>(), OtpFilledCallback,
    MySMSBroadcastReceiver.OTPReceiveListener {
    private var phoneNumber: String? = ""
    private var referralCode: String? = ""
    private lateinit var countDownTimer: CountDownTimer
    private var smsBroadcastReceiver: MySMSBroadcastReceiver? = null

    override val viewModel: OtpVerificationViewModel by viewModel()
    private val signupViewModel: SignUpViewModel by viewModel()

    override fun getLayoutRes(): Int = R.layout.fragment_opt_verification
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        smsBroadcastReceiver = MySMSBroadcastReceiver(this)
    }

    override fun onStart() {
        super.onStart()
        smsBroadcastReceiver.run {
            val intentFilter = IntentFilter()
            intentFilter.addAction("com.google.android.gms.auth.api.phone.SMS_RETRIEVED")
            activity?.registerReceiver(smsBroadcastReceiver, intentFilter)
        }
    }

    override fun onStop() {
        super.onStop()
        smsBroadcastReceiver.run {
            activity?.unregisterReceiver(smsBroadcastReceiver)
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        (activity as BaseActivity<*>).setSupportActionBar(toolbar)
        setHasOptionsMenu(true)
        showBackButton()
        init()
    }

    private fun init() {
        requireActivity().onBackPressedDispatcher.addCallback(this) {
            popBackStack()
        }

        editText_layout.addEditTexts(4, this)
        KeyboardUtil.showKeyboard(activity)
        phoneNumber = arguments?.getString(Constants.PHONE_NUMBER)!!
        referralCode = arguments?.getString(Constants.REFERRAL_CODE)!!
        if (phoneNumber?.startsWith(Constants.PHONE_NUMBER_PREFIX)!!) phoneNumber =
            phoneNumber?.subSequence(3, phoneNumber?.length!!).toString().trim()
        sms_verification_info2.text =
            resources.getString(R.string.sms_verification_text2, phoneNumber)
        sms_verification_info2.makeLinks(Pair("EDIT", View.OnClickListener {
            popBackStack()
        }))
        setObserver()
        btSubmit.setOnClickListener {
            verifyOtp()
        }
        resend_again.setOnClickListener {
            if (resend_again.text == resources.getString(R.string.send_again)) {

                showLoading(true)
                Bundle().apply {
                    FirebaseAnalyticsLog.trackFireBaseEventLog(
                        FirebaseEventNameENUM.FBE_INIT_LOGIN,
                        this
                    )
                }
                val helper: AppSignatureHelper = context?.let { it1 -> AppSignatureHelper(it1) }!!
                signupViewModel.sendMessage(phoneNumber!!, "", helper.getAppSignatures()[0])
                countDownTimer.cancel()
            }
        }
        setCountDownTimer()
    }

    override fun otpFilled() {
        verifyOtp()
    }

    private fun verifyOtp() {
        if (editText_layout.getText().length == 4) {
            showRoundProgressBar(true)
            btSubmit.visibility = View.GONE
            viewModel.verifyOtp(
                phoneNumber = phoneNumber!!,
                otp = editText_layout.getText().toString(),
               oldUserId = Utils.getOldUserId(requireContext())
            )
        } else activity?.showShortToast("Invalid OTP")
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> KeyboardUtil.hideKeyboard(activity)
        }
        return super.onOptionsItemSelected(item)
    }

    private fun setCountDownTimer() {
        countDownTimer = object : CountDownTimer(120000, 1000) {
            override fun onTick(millisUntilFinished: Long) {
                send_again.text =
                    resources.getString(R.string.resend_otp_ts, (millisUntilFinished / 1000))
                if(millisUntilFinished<90*1000){
                resend_again.visibility=View.VISIBLE
               }else{
                    resend_again.visibility=View.INVISIBLE
                }
            }

            override fun onFinish() {
                send_again.text = resources.getString(R.string.expired)
            }
        }
        countDownTimer.start()
    }

    private fun setObserver() {
        signupViewModel.getSignupStatus().observe(viewLifecycleOwner, EventObserver {
            when (it) {
                is OtpState.Success -> {
                    btSubmit.visibility = View.VISIBLE
                    //   context?.showShortToast(it.message)
                    showLoading(false)
                    showRoundProgressBar(false)
                    setCountDownTimer()


                }
                is OtpState.Failure -> {
                    showRoundProgressBar(false)
                    showLoading(false)
                    btSubmit.visibility = View.VISIBLE
                    activity?.showShortToast(it.errorMessage)
                }
            }
        })


        viewModel.getOtpVerificationStatus().observe(viewLifecycleOwner, EventObserver {
            //showRoundProgressBar(false)
            when (it) {
                is LoginState.Success -> {
                    // (activity?.application as App).restartKoin()
                    btSubmit.visibility = View.VISIBLE
                    it.data.let { user ->

                        signupViewModel.setSharedPreferences(
                            Constants.USER_NAME,
                            "${user.firstName ?: ""} ${user.lastName ?: ""}"
                        )
                        signupViewModel.setSharedPreferences(
                            Constants.ACCESS_TOKEN,
                            user.activationKey ?: ""
                        )
                        signupViewModel.setSharedPreferences(Constants.USER_ID, user.userOid ?: "")
                        signupViewModel.setSharedPreferences(
                            Constants.USER_NUMBER,
                            user.mobileNo ?: ""
                        )
                        signupViewModel.setSharedPreferences(
                            Constants.USER_CLASS_ID,
                            user.classX ?: ""
                        )
                        signupViewModel.setSharedPreferences(Constants.USER_DOB, user.dob ?: "")
                        signupViewModel.setSharedPreferences(Constants.USER_TYPE, user.userType ?: "")
                        signupViewModel.setSharedPreferences(Constants.USER_ADDRESS, user.address ?: "")
                        signupViewModel.setSharedPreferences(
                            Constants.USER_GENDER,
                            user.gender ?: ""
                        )
                        signupViewModel.setSharedPreferences(
                            Constants.USER_EMAIL,
                            user.emailAddr ?: ""
                        )
                        val args = Bundle()
                        args.putString(FirebaseEventNameENUM.FBE_MOBILE_NUMBER, user.mobileNo ?: "")
                        args.putString(FirebaseEventNameENUM.FBE_OLD_USER_ID,Utils.getOldUserId(requireContext()))
                        FirebaseAnalyticsLog.trackFireBaseEventLog(FirebaseEventNameENUM.FBE_USER_LOGIN,args)
                        user.userOid?.let {
                            replaceBottomFragment()
                        }?: kotlin.run {
                            var bundle = Bundle()
                            bundle.putString(Constants.MOBILE_NUMBER, phoneNumber)
                            replaceFrament(FragmentFactory.Screens.USER_REGISTRATION_FRAGMENT, bundle)
                        }

                    }
                }
                is LoginState.Failure -> {
                    btSubmit.visibility = View.VISIBLE
                    if (it.errorCode == 201) {

                        var bundle = Bundle()
                        bundle.putString(Constants.MOBILE_NUMBER, phoneNumber)
                        addFrament(FragmentFactory.Screens.USER_REGISTRATION_FRAGMENT, bundle)
                    } else {
                        activity?.showLongToast(it.errorMessage)
                    }
                    showRoundProgressBar(false)

                }
            }
        })
    }

    override fun onDestroyView() {
        super.onDestroyView()
        countDownTimer.cancel()
        showRoundProgressBar(false)
        KeyboardUtil.hideKeyboard(activity)
    }

    override fun onOTPReceived(otp: String) {
        editText_layout.setText(otp)
    }

    override fun onOTPTimeOut() {
    }
}